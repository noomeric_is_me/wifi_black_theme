package wimosalsafiwifimap.activity.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;


/**
 * Created by Federico
 */
public class WifiSecureDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public interface WifiSecureDialogListener {
        void onDialogOpenNetworksClick(DialogFragment dialog);
        void onDialogSecureNetworksClick(DialogFragment dialog);
        void onDialogAllNetworksClick(DialogFragment dialog);
    }

    private WifiSecureDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (WifiSecureDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement WifiSecureDialogListener");
        }
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_secure_title);
        builder.setItems(R.array.secure_array, this);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case 0:
                mListener.onDialogOpenNetworksClick(WifiSecureDialog.this);
                break;
            case 1:
                mListener.onDialogSecureNetworksClick(WifiSecureDialog.this);
                break;
            case 2:
                mListener.onDialogAllNetworksClick(WifiSecureDialog.this);
                break;
        }
    }
}
