package wimosalsafiwifimap.module;

import android.content.Context;
import android.net.wifi.WifiManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import wimosalsafifreewifi.application.AppController;
import wimosalsafiwifimap.adapter.FavouritesAdapter;
import wimosalsafiwifimap.adapter.ScanResultAdapter;
import wimosalsafiwifimap.adapter.controller.SnackBarUndoFavourites;
import wimosalsafiwifimap.adapter.controller.SnackBarUndoMain;
import wimosalsafiwifimap.model.db.DataBaseHandler;
import wimosalsafiwifimap.model.db.sqlite.DataBaseManager;
import wimosalsafiwifimap.model.location.LocationHandler;
import wimosalsafiwifimap.model.wifi.WifiKeeper;
import wimosalsafiwifimap.model.wifi.container.WifiListContainer;
import wimosalsafiwifimap.model.wifi.container.strategy.CurrentWifiList;
import wimosalsafiwifimap.model.wifi.container.strategy.SessionWifiList;
import wimosalsafiwifimap.ui.presenter.DataSetExecutor;
import wimosalsafiwifimap.ui.presenter.DataSetHandler;
import wimosalsafiwifimap.ui.presenter.FilterDelegate;
import wimosalsafiwifimap.utils.ResourceProvider;

/**
 * Created by Federico
 */
@Module
public class DaggerModule {
    private final AppController application;

    public DaggerModule(AppController application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    WifiManager provideWifiManager() {
        return (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
    }

    @Provides
    @Singleton
    CurrentWifiList provideCurrentWifiList() {
        return new CurrentWifiList();
    }

    @Provides
    @Singleton
    SessionWifiList provideSessionWifiList() {
        return new SessionWifiList();
    }

    @Provides
    @Singleton
    LocationHandler provideLocationHandler(Context context, DataBaseManager dataBaseManager) {
        return new LocationHandler(context, dataBaseManager);
    }

    @Provides
    @Singleton
    WifiListContainer provideWifiListContainer(CurrentWifiList currentWifiList, SessionWifiList sessionWifiList) {
        return new WifiListContainer(currentWifiList, sessionWifiList);
    }

    @Provides
    @Singleton
    WifiKeeper provideWifiKeeper(WifiListContainer wifiListContainer) {
        return new WifiKeeper(wifiListContainer);
    }

    @Provides
    @Singleton
    DataBaseManager provideDataBaseManager(Context context) {
        return new DataBaseManager(context);
    }

    @Provides
    @Singleton
    DataBaseHandler provideDataBaseHandler(DataBaseManager dataBaseManager, LocationHandler locationHandler, WifiKeeper wifiKeeper) {
        return new DataBaseHandler(dataBaseManager, locationHandler, wifiKeeper);
    }

    @Provides
    @Singleton
    SnackBarUndoMain provideSnackBarUndoMain(DataBaseHandler dataBaseHandler) {
        return new SnackBarUndoMain(dataBaseHandler);
    }

    @Provides
    @Singleton
    SnackBarUndoFavourites provideSnackBarUndoFavourites(DataBaseHandler dataBaseHandler) {
        return new SnackBarUndoFavourites(dataBaseHandler);
    }

    @Provides
    @Singleton
    ResourceProvider provideResourceProvider(WifiKeeper wifiKeeper, DataBaseHandler dataBaseHandler) {
        return new ResourceProvider(wifiKeeper, dataBaseHandler);
    }

    @Provides
    @Singleton
    ScanResultAdapter provideScanResultAdapter(WifiKeeper wifiKeeper, SnackBarUndoMain snackBarUndoMain, ResourceProvider resourceProvider) {
        return new ScanResultAdapter(wifiKeeper, snackBarUndoMain, resourceProvider);
    }

    @Provides
    @Singleton
    FavouritesAdapter provideFavouritesAdapter(DataBaseHandler dataBaseHandler, SnackBarUndoFavourites snackBarUndoFavourites, ResourceProvider resourceProvider) {
        return new FavouritesAdapter(dataBaseHandler, snackBarUndoFavourites, resourceProvider);
    }

    @Provides
    @Singleton
    FilterDelegate provideFilterDelegate(WifiKeeper wifiKeeper, ScanResultAdapter scanResultAdapter) {
        return new FilterDelegate(wifiKeeper, scanResultAdapter);
    }

    @Provides
    @Singleton
    DataSetExecutor provideDataSetExecutor(WifiKeeper wifiKeeper, DataBaseHandler dataBaseHandler, LocationHandler locationHandler, WifiManager wifiManager) {
        return new DataSetExecutor(wifiKeeper, dataBaseHandler, locationHandler, wifiManager);
    }

    @Provides
    @Singleton
    DataSetHandler provideDataSetHandler(DataSetExecutor dataSetExecutor, FavouritesAdapter favouritesAdapter, ScanResultAdapter scanResultAdapter) {
        return new DataSetHandler(dataSetExecutor, favouritesAdapter, scanResultAdapter);
    }
}
