package wimosalsafiwifimap.model.wifi.container.strategy;

import java.util.List;

import wimosalsafiwifimap.model.wifi.WifiElement;

/**
 * Created by Federico
 */
public interface WifiListPopulate {
    void populate(List<WifiElement> wifiElementList);
}
