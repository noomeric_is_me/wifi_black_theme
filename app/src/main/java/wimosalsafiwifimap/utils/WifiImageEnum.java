package wimosalsafiwifimap.utils;


import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

/**
 * Created by Federico
 */
public enum WifiImageEnum {
    LEVEL_0 {
        public int getResource() {
            return R.drawable.signal_wifi1;
        }
    },
    LEVEL_1 {
        public int getResource() {
            return R.drawable.signal_wifi2;
        }
    },
    LEVEL_2 {
        public int getResource() {
            return R.drawable.signal_wifi3;
        }
    },
    LEVEL_3 {
        public int getResource() {
            return R.drawable.signal_wifi4;
        }
    };

    abstract int getResource();
}
