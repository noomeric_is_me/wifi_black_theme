package wimosalsafiwifimap.utils;


import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

/**
 * Created by Federico
 */
public enum WifiSecureImageEnum {
    LEVEL_0_WPA {
        public int getResource() {
            return R.drawable.signal_wifi1;
        }
    },
    LEVEL_1_WPA {
        public int getResource() {
            return R.drawable.signal_wifi2;
        }
    },
    LEVEL_2_WPA {
        public int getResource() {
            return R.drawable.signal_wifi3;
        }
    },
    LEVEL_3_WPA {
        public int getResource() {
            return R.drawable.signal_wifi4;
        }
    };

    abstract int getResource();
}
