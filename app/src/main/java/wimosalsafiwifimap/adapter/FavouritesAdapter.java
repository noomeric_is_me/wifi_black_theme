package wimosalsafiwifimap.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wimosalsafiwifimap.adapter.controller.SnackBarShowUndo;
import wimosalsafiwifimap.adapter.controller.SnackBarUndoFavourites;
import wimosalsafiwifimap.model.db.DataBaseHandler;
import wimosalsafiwifimap.model.wifi.WifiElement;
import wimosalsafiwifimap.utils.ResourceProvider;

/**
 * Created by Federico
 */
public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.ViewHolder> {

    private final DataBaseHandler dataBaseHandler;
    private final SnackBarUndoFavourites snackBarUndoFavourites;
    private final ResourceProvider resourceProvider;

    @Inject
    public FavouritesAdapter(DataBaseHandler dataBaseHandler, SnackBarUndoFavourites snackBarUndoFavourites, ResourceProvider resourceProvider) {
        this.dataBaseHandler = dataBaseHandler;
        this.snackBarUndoFavourites = snackBarUndoFavourites;
        this.resourceProvider = resourceProvider;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.wifi_map_list_item, parent, false);
        return new ViewHolder(itemLayoutView, snackBarUndoFavourites, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        WifiElement wifiElement = dataBaseHandler.get(position);

        viewHolder.wifiElement = wifiElement;
        viewHolder.textViewTitle.setText(wifiElement.getSSID());
        viewHolder.textViewBSSID.setText(wifiElement.getBSSID());
        viewHolder.textViewDetail.setText(wifiElement.getCapabilities());
        viewHolder.imgViewIcon.setImageResource(resourceProvider.getWifiResource(wifiElement));
        viewHolder.imgSaveIcon.setImageResource(resourceProvider.getSavedResource());
    }

    @Override
    public int getItemCount() {
        return dataBaseHandler.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final SnackBarShowUndo snackBarShowUndo;
        private final RecyclerView.Adapter adapter;

        public WifiElement wifiElement;

        @BindView(R.id.firstLine)
        public TextView textViewTitle;

        @BindView(R.id.secondLine)
        public TextView textViewBSSID;

        @BindView(R.id.thirdLine)
        public TextView textViewDetail;

        @BindView(R.id.network_info_img)
        public ImageView imgViewIcon;

        @BindView(R.id.saveicon)
        public ImageView imgSaveIcon;

        public ViewHolder(View view, SnackBarShowUndo snackBarShowUndo, RecyclerView.Adapter adapter) {
            super(view);
            ButterKnife.bind(this, view);

            this.snackBarShowUndo = snackBarShowUndo;
            this.adapter = adapter;

            view.setOnClickListener(this);
        }

        @OnClick(R.id.saveicon)
        public void onClickSave(View view) {
            snackBarShowUndo.showUndo(adapter, (RecyclerView) view.getParent().getParent(), wifiElement);
        }

        @Override
        public void onClick(View v) {
//            Context context = v.getContext();
//            Intent intent = new Intent(context, DetailActivity.class);
//            intent.putExtra(DetailActivity.EXTRA_NAME, wifiElement);
//
//            context.startActivity(intent);
        }
    }
}
