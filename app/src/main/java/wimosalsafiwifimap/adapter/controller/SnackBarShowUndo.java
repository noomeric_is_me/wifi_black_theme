package wimosalsafiwifimap.adapter.controller;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import wimosalsafiwifimap.model.wifi.WifiElement;


/**
 * Created by Federico
 */
public interface SnackBarShowUndo {
    void showUndo(RecyclerView.Adapter adapter, View view, WifiElement wifiElement);
}
