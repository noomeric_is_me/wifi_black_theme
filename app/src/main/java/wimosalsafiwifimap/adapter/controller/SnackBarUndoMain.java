package wimosalsafiwifimap.adapter.controller;

import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import javax.inject.Inject;

import wimosalsafiwifimap.model.db.DataBaseHandler;
import wimosalsafiwifimap.model.db.sqlite.DataBaseElement;
import wimosalsafiwifimap.model.wifi.WifiElement;

/**
 * Created by federico on 16/11/15.
 */
public class SnackBarUndoMain implements View.OnClickListener, SnackBarShowUndo {
    private final DataBaseHandler dataBaseHandler;

    private RecyclerView.Adapter adapter;
    private View view;
    private DataBaseElement dataBaseElement;

    @Inject
    public SnackBarUndoMain(DataBaseHandler dataBaseHandler) {
        this.dataBaseHandler = dataBaseHandler;
    }

    @Override
    public void showUndo(RecyclerView.Adapter adapter, View view, WifiElement wifiElement) {
        this.adapter = adapter;
        this.view = view;

        dataBaseElement = dataBaseHandler.toggleSave(wifiElement);
        adapter.notifyDataSetChanged();

        if(dataBaseElement == null) {
            Snackbar.make(view, R.string.saved_wifi_element, Snackbar.LENGTH_SHORT).show();
        }
        else {
            Snackbar.make(view, R.string.removed_wifi_element, Snackbar.LENGTH_INDEFINITE).setAction(R.string.undo, this).show();
        }
    }

    @Override
    public void onClick(View v) {
        dataBaseHandler.restore(dataBaseElement);
        adapter.notifyDataSetChanged();
        Snackbar.make(view, R.string.restored_wifi_element, Snackbar.LENGTH_SHORT).show();
    }
}
