package wimosalsafifreewifi.application;

import android.content.Context;
import androidx.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import utils.AppOpenManager;
import wimosalsafiwifimap.component.DaggerWifiExplorerComponent;
import wimosalsafiwifimap.component.WifiExplorerComponent;
import wimosalsafiwifimap.module.DaggerModule;

/**
 * Created by NTL on 1/22/2017 AD.
 */

public class AppController extends MultiDexApplication {
    private RequestQueue mRequestQueue;
    private Context mContext;

    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;
    private WifiExplorerComponent component;
    AppOpenManager appOpenManager;
    @Override
    public void onCreate() {
        super.onCreate();

//        FacebookSdk.sdkInitialize(getApplicationContext());

//        // Set up Crashlytics, disabled for debug builds
//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
//                .build();
//
//        // Initialize Fabric with the debug-disabled crashlytics.
//        Fabric.with(this, crashlyticsKit);

        mContext = this;
        mInstance = this;

        component = DaggerWifiExplorerComponent.builder()
                .daggerModule(new DaggerModule(this))
                .build();
        component.inject(this);
        MobileAds.initialize(
                this,
                new OnInitializationCompleteListener() {
                    @Override
                    public void onInitializationComplete(InitializationStatus initializationStatus) {}
                });
        MobileAds.initialize(
                this,
                new OnInitializationCompleteListener() {
                    @Override
                    public void onInitializationComplete(InitializationStatus initializationStatus) {}
                });
        appOpenManager = new AppOpenManager(this);  //Turn off Appopen ads
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public Context getAppContext() {
        return mContext;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty

        req.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public WifiExplorerComponent getComponent() {
        return component;
    }
}
