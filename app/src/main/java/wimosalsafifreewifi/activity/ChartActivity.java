package wimosalsafifreewifi.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import wimosalsafifreewifi.main.MainAppActivity_v2;
import wimosalsafispeedtest.activity.OptimizeActivity;
import utils.AppUtils;

/**
 * Created by NTL on 9/19/2017 AD.
 */

public class ChartActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Wi-Fi Analyzer");
//            getSupportActionBar().setTitle("Wi-Fi Analyzer");
//            toolbar.setLogo(R.drawable.logo_white);
            toolbar.setContentInsetStartWithNavigation(0);
        }

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        TextView wifi_chart_boost_speed = (TextView) findViewById(com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R.id.wifi_chart_boost_speed);
        wifi_chart_boost_speed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("Analyzer Activity - Wifi Speed Booster"));

                    Intent intent = new Intent(ChartActivity.this, OptimizeActivity.class);
                    intent.putExtra(MainAppActivity_v2.COLOR_MESSAGE, MainAppActivity_v2.currentColor);
                    startActivity(intent);
                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            finish();
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

        super.onBackPressed();
    }
}