package wimosalsafifreewifi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Window;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.snackbar.Snackbar;
import com.thefinestartist.Base;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import java.util.Objects;

import wimosalsafifreewifi.main.MainAppActivity_v2;


public class Splashscreen extends AppCompatActivity {
    CoordinatorLayout coordinatorLayout;
    private static final int LONG_DURATION_MS = 2750;
    LottieAnimationView lottieAnimationView;

    private InterstitialAd mInterstitialAd;
    // Remote Config keys

    private static final String CONFIG_KEY_NATIVE_LARGE_DISPLAY = "display_native_large";
    private static final String CONFIG_KEY_NATIVE_SMALL_DISPLAY = "display_native_small";
    private static final String CONFIG_KEY_FULL_OPEN_APP_DISPLAY = "display_full_open_app";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        coordinatorLayout = findViewById(R.id.cordi);
        lottieAnimationView = findViewById(R.id.animation_view);


//        if (!isOnline(getApplicationContext())) {
//
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    Intent i=new Intent(Splashscreen.this, MainAppActivity_v2.class);
//                    startActivity(i);
//                    finish();
//                    showInterstitial();
//                }
//            }, 3000);
//            Snackbar snackbar2 = Snackbar
//                    .make(coordinatorLayout, "Check internet connection", Snackbar.LENGTH_LONG);
//            snackbar2.show();
//
//
//        } else {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    Intent i=new Intent(Splashscreen.this, MainAppActivity_v2.class);
//                    startActivity(i);
//                    finish();
//                    showInterstitial();
//                }
//            }, 3000);
////            startActivity(new Intent(this, IntroActivity.class));
////            finish();
//        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i=new Intent(Splashscreen.this, MainAppActivity_v2.class);
                startActivity(i);
                finish();
                showInterstitial();
            }
        }, 5000);
//        //Toast.makeText(this, "initialize in Splash", //Toast.LENGTH_SHORT).show();
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Loading , Please wait...", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_interstitial_splash_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });
        Base.initialize(this);
    }


    public void showInterstitial() {

            //show native
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
//                Toast.makeText(getActivity(), "CONFIG_KEY_NATIVE_LARGE_DISPLAY - True", Toast.LENGTH_SHORT).show();

    }

    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = Objects.requireNonNull(cm).getActiveNetworkInfo();
            return nInfo != null && nInfo.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}