package wimosalsafifreewifi.services.model;

/**
 * Created by NTL on 1/22/2017 AD.
 */

import java.util.List;

public class data {

    private String defaultads;
    private String packagename;
    private int ads_interstitial_count;
    private boolean ads_interstitial_show_all;
    private List<Ads> ads = null;
    private SpeedTestSharing speedTestSharing;

    public String getDefaultads() {
        return defaultads;
    }

    public void setDefaultads(String defaultads) {
        this.defaultads = defaultads;
    }

    public String getPackagename() {
        return packagename;
    }

    public data withDefaultads(String defaultads) {
        this.defaultads = defaultads;
        return this;
    }

    public List<Ads> getAds() {
        return ads;
    }

    public void setAds(List<Ads> ads) {
        this.ads = ads;
    }

    public data withAds(List<Ads> ads) {
        this.ads = ads;
        return this;
    }

    public SpeedTestSharing getSpeedTestSharing() {
        return speedTestSharing;
    }

    public void setSpeedTestSharing(SpeedTestSharing speedTestSharing) {
        this.speedTestSharing = speedTestSharing;
    }

    public int getAds_interstitial_count() {
        return ads_interstitial_count;
    }

    public boolean isAds_interstitial_show_all() {
        return ads_interstitial_show_all;
    }
}