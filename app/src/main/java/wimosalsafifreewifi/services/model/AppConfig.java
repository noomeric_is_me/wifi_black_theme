package wimosalsafifreewifi.services.model;

/**
 * Created by NTL on 1/22/2017 AD.
 */


public class AppConfig {

    private data data;

    public data getData() {
        return data;
    }

    public void setData(data data) {
        this.data = data;
    }

    public AppConfig withData(data data) {
        this.data = data;
        return this;
    }

}