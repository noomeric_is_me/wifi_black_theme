package wimosalsafifreewifi.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;
import com.thefinestartist.finestwebview.FinestWebView;

import wimosalsafifreewifi.activity.WifiConnectActivity;
import wimosalsafifreewifi.application.AppController;
import wimosalsafispeedtest.activity.OptimizeActivity;
import utils.AppUtils;


/**
 * Created by florentchampigny on 27/05/2016.
 */
public class DrawerActivity extends AppCompatActivity {

    protected DrawerLayout mDrawer;
    protected ActionBarDrawerToggle mDrawerToggle;
    private Handler mHandler;
    protected static final int NAVDRAWER_LAUNCH_DELAY = 250;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();


        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, 0, 0);
        mDrawer.setDrawerListener(mDrawerToggle);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
//            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_48dp);
        }
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        switch (item.getItemId()) {
//            case android.R.id.home:
//                mDrawer.openDrawer(GravityCompat.START);
//                return true;
//
//            case R.id.action_wifi_gift:
////                Toast.makeText(this,"action_refresh",Toast.LENGTH_SHORT).show();
//                try {
//                    mHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getinstnce().showAdmobAdsFullBanner(null);
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getinstnce().showFBAdsFullBanner(null);
//                            }
//                        }
//                    }, 200);
//                } catch (Exception ignored) {
//                    Crashlytics.logException(ignored);
//                }
//
//                return true;
//        }
//        return super.onOptionsItemSelected(item);

        return mDrawerToggle.onOptionsItemSelected(item) ||
                super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    protected void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                goToNavDrawerItem(menuItem.getItemId());
                            }
                        }, NAVDRAWER_LAUNCH_DELAY);

//                        menuItem.setChecked(true);
                        mDrawer.closeDrawers();
                        return true;
                    }
                });
    }

    private void showDialogThankyou(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DrawerActivity.this);
        alertDialogBuilder.setTitle("Thank you !");
        alertDialogBuilder.setMessage("Thank you for your support and give us a feedback we promise to improve as soon as possible.");
        alertDialogBuilder.setPositiveButton("Dismiss",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        alertDialogBuilder.show();
    }

    private void goToNavDrawerItem(int menuItem) {
        final Intent intent;
        String setContentUrl = "";
        String setImageUrl = "";

        switch (menuItem) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return;

//            case R.id.action_wifi_gift:
//                Toast.makeText(this,"action_refresh",Toast.LENGTH_SHORT).show();
//                break;

            case R.id.nav_wifi_find:
                try {
                    intent = new Intent(DrawerActivity.this, WifiConnectActivity.class);
                    startActivity(intent);
                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

//            case R.id.nav_wifi_analyzer:
//                try {
//                    intent = new Intent(DrawerActivity.this, ChartActivity.class);
//                    startActivity(intent);
//                } catch (Exception ignored) {
//                    Crashlytics.logException(ignored);
//                }
//                break;
            case R.id.nav_speed_booster:
                try {
                    intent = new Intent(DrawerActivity.this, OptimizeActivity.class);
                    intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                    startActivity(intent);
                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }
                break;

//            case R.id.nav_wifi_map:
//
//                break;

            case R.id.nav_share:

                try{
                    if(AppUtils.appConfig != null){

                        setContentUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_contentUrl();
                        setImageUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_setImageUrl();

                    } else{
                        setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                        setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                    }

                }catch (Exception e){
                    Crashlytics.logException(e);
                    setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                    setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                }

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-ShareAppToFacebook", "Click"));

                try{
                    ShareDialog shareDialog = new ShareDialog(DrawerActivity.this);

                    if (ShareDialog.canShow(ShareLinkContent.class)) {

                        ShareLinkContent content = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(setContentUrl))
                                .setImageUrl(Uri.parse(setImageUrl))
                                .setContentTitle(DrawerActivity.this.getString(R.string.share_setTitle))
                                .setContentDescription(DrawerActivity.this.getString(R.string.share_setContentDescription))
                                .build();
                        shareDialog.show(content);
                    }
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(DrawerActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;

//            case R.id.nav_rating:
//
//                Answers.getInstance().logCustom(new CustomEvent("About")
//                        .putCustomAttribute("About-RateOnPlayStore", "Click"));
//
//                AppUtils.getinstnce().openPlaystore(DrawerActivity.this);
//                break;

            case R.id.nav_feedback:

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-Feedback", "Click"));

                try{
                    final RatingDialog ratingDialog = new RatingDialog.Builder(DrawerActivity.this)
                            .threshold(3)
                            .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                                @Override
                                public void onFormSubmitted(String feedback) {

                                    Answers.getInstance().logCustom(new CustomEvent("About")
                                            .putCustomAttribute("About-Feedback", feedback));

                                    showDialogThankyou();
                                }
                            })
                            .build();
                    ratingDialog.show();
                    ratingDialog.openForm();

                }catch (Exception e){

                    Crashlytics.logException(e);
                    Toast.makeText(DrawerActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_visit_website:

                try{
                    if(AppUtils.appConfig != null){

                        setContentUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_contentUrl();
                        setImageUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                .get_share_setImageUrl();

                    } else{
                        setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                        setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                    }

                }catch (Exception e){
                    Crashlytics.logException(e);
                    setContentUrl = AppController.getInstance().getAppContext().getString(R.string.share_contentUrl);
                    setImageUrl = AppController.getInstance().getAppContext().getString(R.string.share_setImageUrl);
                }

                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-Website", "Click"));

                try{

                    new FinestWebView.Builder(DrawerActivity.this)
//                            .toolbarColor(color_message)
//                            .statusBarColor(color_message)
//                            .swipeRefreshColor(color_message)
                            .disableIconMenu(true)
                            .showUrl(false)
                            .show(setContentUrl);
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(DrawerActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.nav_privacy_policy:
                Answers.getInstance().logCustom(new CustomEvent("About")
                        .putCustomAttribute("About-Privacy", "Click"));

                try{
                    String url_privacy_policy = DrawerActivity.this.getResources().getString(R.string.url_privacy_policy);
                    new FinestWebView.Builder(DrawerActivity.this)
//                            .toolbarColor(color_message)
//                            .statusBarColor(color_message)
//                            .statusBarColor(color_message)
                            .disableIconMenu(true)
                            .showUrl(false)
                            .show(url_privacy_policy);
                }catch (Exception e){
                    Crashlytics.logException(e);
                    Toast.makeText(DrawerActivity.this
                            , "something went wrong, we will fix this as soon as possible."
                            , Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
}
