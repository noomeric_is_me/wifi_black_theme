package wimosalsafifreewifi.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import io.github.dreierf.materialintroscreen.SlideFragment;

public class CustomSlide extends SlideFragment {
    private Button mButton;
    private Context mContext;
    private static boolean mSettingPermission = false;

    public static boolean getMSettingPermission() {
        return mSettingPermission;
    }
    public static void setMSettingPermission(boolean mSettingPermission) {
        CustomSlide.mSettingPermission = mSettingPermission;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_custom_slide, container, false);
        mButton = view.findViewById(R.id.button1);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                WifiApManager wifiApManager = new WifiApManager(getContext());
                // force to show the settings page for demonstration purpose of this method
                showWritePermissionSettings(true);
            }
        });

        mContext = this.getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        try {
            boolean allow_write_settings = Settings.System.canWrite(mContext);
            if(allow_write_settings){
                mButton.setEnabled(false);
                mButton.setText("ACCEPTED");
            } else {
                mButton.setEnabled(true);
                mButton.setText("ACCEPT");
            }
        } catch (Exception ignored) {

        }
    }
    public void showWritePermissionSettings(boolean force) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (force || !Settings.System.canWrite(mContext)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + mContext.getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        }
    }
    @Override
    public boolean hasNeededPermissionsToGrant() {
        return super.hasNeededPermissionsToGrant();
    }

    @Override
    public int backgroundColor() {
        return R.color.custom_slide_background;
    }

    @Override
    public int buttonsColor() {
        return R.color.custom_slide_buttons;
    }

    @Override
    public boolean canMoveFurther() {
        return Settings.System.canWrite(mContext);
    }

    @Override
    public String cantMoveFurtherErrorMessage() {
//        return getString(R.string.error_message);
        return "Please accept permission first.";
    }
}
//public class CustomSlide extends SlideFragment {
//    static final int MY_PERMISSIONS_MANAGE_WRITE_SETTINGS = 100 ;
//    private Button button;
//    private View view;
//    private boolean mSettingPermission = true;
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        final View view = inflater.inflate(R.layout.fragment_custom_slide, container, false);
//        Button button = (Button) view.findViewById(R.id.button1);
//        if (!Settings.System.canWrite(getContext())) {
//            button.setText("ACCEPT");
//        } else {
//            button.setText("ACCEPTED");
//            button.setEnabled(false);
//        }
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                settingPermission();
//            }
//        });
//
//        return view;
//    }
//
//    @Override
//    public int backgroundColor() {
//        return R.color.custom_slide_background;
//    }
//
//    @Override
//    public int buttonsColor() {
//        return R.color.custom_slide_buttons;
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public boolean canMoveFurther() {
//        return Settings.System.canWrite(getContext());
//    }
//
//    @Override
//    public String cantMoveFurtherErrorMessage() {
//        return getString(R.string.error_message);
//    }
//
//
//    private static int requestCode_ACTION_MANAGE_WRITE_SETTINGS = 200;
//    private void settingPermission() {
//        mSettingPermission = true;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (!Settings.System.canWrite(getContext())) {
//                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getContext().getPackageName()));
//                startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
//                return;
//            }
//        }
//    }
////    @Override
////    public void onActivityResult(int requestCode, int resultCode, Intent data) {
////        // Check which request we're responding to
////        if (requestCode == MY_PERMISSIONS_MANAGE_WRITE_SETTINGS) {
////            // Make sure the request was successful
////            if (resultCode == RESULT_OK) {
////                mSettingPermission = true;
////            } else {
////                settingPermission();
////            }
////        }
////
//////        if (mSettingPermission) onPermissionsOkay();
////
////    }
//
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        mSettingPermission = true;
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == requestCode_ACTION_MANAGE_WRITE_SETTINGS){
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (!Settings.System.canWrite(getContext())) {
//                    mSettingPermission = true;
//                } else {
//                    mSettingPermission = false;
////                    button.setText("ACCEPTED");
////                    button.setEnabled(false);
//                    Toast.makeText(getContext(),"Permission Accepted", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }
//    }
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public void onResume() {
//        //other stuff
//        super.onResume();
//        if (!Settings.System.canWrite(getContext())) {
//
////            Toast.makeText(getContext(),"No", Toast.LENGTH_SHORT).show();
//        } else {
//
////            Toast.makeText(getContext(),"Yes", Toast.LENGTH_SHORT).show();
//        }
//    }
//    @Override
//    public void onPause() {
//        super.onPause();
////        Toast.makeText(getContext(),"onPause", Toast.LENGTH_SHORT).show();
//    }
//
//}