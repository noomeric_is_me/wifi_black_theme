package wimosalsafifreewifi.main;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class PagerAdapter extends FragmentPagerAdapter {

	private String[] CONTENT;
	private Fragment[] fragment = new Fragment[0];
    private boolean isChangeToArrayList;
    ArrayList<Fragment> mArrayFragment;
    ArrayList<String> mArrayContent;

	public PagerAdapter(FragmentManager fm, Fragment[] arrayFragment, String[] content, Context _context) {
		super(fm);
        isChangeToArrayList = false;
		CONTENT = content;
		fragment = arrayFragment;
	}


    public PagerAdapter(FragmentManager fm, ArrayList<Fragment> arrayFragment, ArrayList<String> content, Context _context){
        super(fm);
        isChangeToArrayList = true;
        mArrayFragment = arrayFragment;
        mArrayContent = content;

    }

	@Override
	public Fragment getItem(int position) {
        if(isChangeToArrayList){
            return mArrayFragment.get(position);
        }else {
		return fragment[position];
        }
	}

	@Override
	public int getCount() {
        if(isChangeToArrayList){
            return mArrayContent.size();
        }else {
		return CONTENT.length;
        }
	}

	@Override
	public CharSequence getPageTitle(int position) {
        if(isChangeToArrayList){
            return mArrayContent.get(position);
        }else{

		return CONTENT[position];
        }
	}



}