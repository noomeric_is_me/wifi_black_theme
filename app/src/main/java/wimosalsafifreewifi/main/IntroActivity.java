package wimosalsafifreewifi.main;

import android.os.Bundle;
import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import android.view.View;

import com.thefinestartist.Base;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import io.github.dreierf.materialintroscreen.MaterialIntroActivity;
import io.github.dreierf.materialintroscreen.SlideFragmentBuilder;
import io.github.dreierf.materialintroscreen.animations.IViewTranslation;

public class IntroActivity extends MaterialIntroActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Base.initialize(this);

        enableLastSlideAlphaExitTransition(true);

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

//        addSlide(new SlideFragmentBuilder()
//                        .backgroundColor(R.color.third_slide_background)
//                        .buttonsColor(R.color.third_slide_buttons)
//                        .neededPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
//                        .image(R.drawable.slide_4)
//                        .title("Please allow access to Location")
//                        .description("To find WiFi around you,\n You need to provide location access to this app")
//                        .build(),
//                new MessageButtonBehaviour(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showMessage("Thank you!");
//                    }
//                }, "Already grant permission"));


        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.first_slide_background)
                        .buttonsColor(R.color.first_slide_buttons)
                        .image(R.drawable.slide_1)
                        .title("Enables Your Phone")
                        .description("to search for open WiFi network\nand connect to it automatically !")
                        .build());

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.second_slide_background)
                .buttonsColor(R.color.second_slide_buttons)
                .image(R.drawable.slide_2)
                .title("Introducing New Feature\nSpeed Test Plus")
                .description("Would you try SpeedTest+ ?")
                .build());


        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.third_slide_background)
                .buttonsColor(R.color.third_slide_buttons)
                .image(R.drawable.slide_0)
                .title("Enjoy\n\nFree WiFi Everywhere !")
                .description("Go on, Let's fun !")
                .build());
    }

    @Override
    public void onFinish() {
        super.onFinish();
//        Toast.makeText(this, "Try this library in your project! :)", Toast.LENGTH_SHORT).show();
    }
}