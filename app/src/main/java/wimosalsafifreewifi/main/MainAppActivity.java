/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package wimosalsafifreewifi.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.astuetz.PagerSlidingTabStrip;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.BuildConfig;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;
import com.codemybrainsout.ratingdialog.RatingDialog;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mady.wifi.api.WifiStatus;
import com.mady.wifi.api.wifiHotSpots;
import com.rampo.updatechecker.UpdateChecker;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.fragment.ChartFragment;
import wimosalsafifreewifi.fragment.WifiConnectFragment_v1;
import wimosalsafifreewifi.services.GsonRequest;
import wimosalsafifreewifi.services.ServiceAPI;
import wimosalsafifreewifi.services.model.AppConfig;
import wimosalsafifreewifi.services.model.External_IP;
import wimosalsafifreewifi.services.model.NetworkInformation;
import io.fabric.sdk.android.Fabric;
import wimosalsafimainapp.activity.AboutActivity;
import wimosalsafispeedtest.activity.SpeedTestHistoryActivity;
import wimosalsafiwifimap.fragment.SpeedMapFragment;
import wimosalsafispeedtest.fragment.SpeedTestMiniFragment;
import utils.AppUtils;
import utils.CustomViewPager;

public class MainAppActivity extends AppCompatActivity {

    public static final String TAG = MainAppActivity.class.getSimpleName();
    public static String COLOR_MESSAGE = "COLOR_MESSAGE";
    private static int show_rating_dialog_countdown = 1;

    private boolean canExitAppWithOutRateApp, isShowSpeedTestIntro,isShowWifiHotspotIntro,isShowCloseWifiHotspotIntro;

    private PagerSlidingTabStrip tabs;
    private CustomViewPager pager;

    //    private MyPagerAdapter adapter;
    private PagerAdapter adapter;
    private Drawable oldBackground = null;
    public static int currentColor;
    private WifiConnectFragment_v1 wifiConnectFragment;
    private SpeedTestMiniFragment speedTestMiniFragment;
    private SpeedMapFragment speedMapFragment;
    private ChartFragment chartFragment;
    private SharedPreferences sharedPref;
    private CallbackManager callbackManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Handler mHandler;
    private Menu mMenu;
    private static String wifi_lable_tab1 = "Free WiFi-Hotspot";
    private static String wifi_lable_tab2 = "Speed & Booster";
    private static String wifi_lable_tab3 = "Network Map";
    //    public static boolean isSpeedbooster_active;
    private boolean mRequestAdsConfig;
    private wifiHotSpots mHotUtil;
    private WifiStatus mWifiStatus;
    public static boolean mWifiHotspotEnable;
    private int i = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mHandler = new Handler();

//        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
//        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);


        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);

        setContentView(R.layout.pageslide);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pager = (CustomViewPager) findViewById(R.id.pager);


        ArrayList<String> mArrayListContent = new ArrayList<>();
        ArrayList<Fragment> arrayListFragment = new ArrayList<>();

        ArrayList<String> atArrayList = new ArrayList<String>();

        wifiConnectFragment = new WifiConnectFragment_v1();
        arrayListFragment.add(wifiConnectFragment);
        mArrayListContent.add(wifi_lable_tab1);
        atArrayList.add(wifi_lable_tab1);

//        chartFragment = new ChartFragment();
//        arrayListFragment.add(chartFragment);
//        mArrayListContent.add("Graphs");
//        atArrayList.add("Graphs");

        speedTestMiniFragment = new SpeedTestMiniFragment();
        arrayListFragment.add(speedTestMiniFragment);
        mArrayListContent.add(wifi_lable_tab2);
        atArrayList.add(wifi_lable_tab2);

        speedMapFragment = new SpeedMapFragment();
        arrayListFragment.add(speedMapFragment);
        mArrayListContent.add(wifi_lable_tab3);
        atArrayList.add(wifi_lable_tab3);

        adapter = new PagerAdapter(getSupportFragmentManager(), arrayListFragment, mArrayListContent, MainAppActivity.this);
        pager.setAdapter(adapter);
        pager.setPagingEnabled(true);
        tabs.setViewPager(pager);

        changeColor(ContextCompat.getColor(getBaseContext(), R.color.green));

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
//                throw new RuntimeException("This is a crash");

                changeColor(AppUtils.getInstance().getRandomColor(currentColor));

                try {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if(AppUtils.ads_interstitial_show_all) {

                                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                                    AppUtils.getInstance().showAdsFullBanner(null);
                                }

                            }else {

                                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                                    AppUtils.getInstance().showAdmobAdsFullBanner(null);
                                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                                    AppUtils.getInstance().showFBAdsFullBanner(null);
                                }
                            }

                        }
                    }, 200);
                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }


                if(mMenu != null){
//                    if(position == 0 || position == 1) {
                    if(position == 0) {
                        mMenu.findItem(R.id.action_refresh).setVisible(true);
                        mMenu.findItem(R.id.speedtest_history).setVisible(false);
                        mMenu.findItem(R.id.wifi_hotspot).setVisible(false);
                    } else if (position == 1){
                        mMenu.findItem(R.id.action_refresh).setVisible(false);
                        mMenu.findItem(R.id.speedtest_history).setVisible(true);
                        mMenu.findItem(R.id.wifi_hotspot).setVisible(false);

                        if(!isShowSpeedTestIntro) {
                            //Show speed test history intro
                            TextView textview_mock = (TextView) findViewById(R.id.toolbar_textview_mock);
                            showIntroSpeedtestHistory(textview_mock);
                        }
                    } else if (position == 2){
                        mMenu.findItem(R.id.action_refresh).setVisible(false);
                        mMenu.findItem(R.id.speedtest_history).setVisible(false);
                        mMenu.findItem(R.id.wifi_hotspot).setVisible(true);

                        if(!isShowWifiHotspotIntro) {
                            //Show speed test history intro
                            TextView textview_mock = (TextView) findViewById(R.id.toolbar_textview_mock);
                            showIntroWifiHotspot(textview_mock);
                        }
                    } else {
                        mMenu.findItem(R.id.speedtest_history).setVisible(false);
                        mMenu.findItem(R.id.action_refresh).setVisible(false);
                        mMenu.findItem(R.id.wifi_hotspot).setVisible(false);
                    }
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Read last config
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        AppUtils.adsNetworkType = sharedPref.getInt(AppUtils.ADS_TYPE_KEY, AppUtils.FB_ADS_TYPE);
//        AppUtils.adsNetworkType = AppUtils.FB_ADS_TYPE;
//        AppUtils.adsNetworkType = AppUtils.ADMOB_ADS_TYPE;
//        AppUtils.adsNetworkType = AppUtils.NONE_TYPE;


        //Rating App
        canExitAppWithOutRateApp = sharedPref.getBoolean(AppUtils.RATING_APP_TYPE_KEY, false);

        if(!canExitAppWithOutRateApp){
            int show_rate_dialog_count = sharedPref.getInt(AppUtils.RATING_APP_COUNT_TYPE_KEY,show_rating_dialog_countdown);

            if(show_rate_dialog_count > 0) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(AppUtils.RATING_APP_COUNT_TYPE_KEY, show_rate_dialog_count - 1);
                editor.apply();

                canExitAppWithOutRateApp = true; //Force can exit app without rating
            }else{
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(AppUtils.RATING_APP_COUNT_TYPE_KEY, show_rating_dialog_countdown);
                editor.apply();
            }
        }

        //Show Speed History Intro
        isShowSpeedTestIntro = sharedPref.getBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_HISTORY_TYPE_KEY, false);

        //Show Wifi Hotspot Intro
        isShowWifiHotspotIntro = sharedPref.getBoolean(AppUtils.SHOW_INTRO_WIFI_HOTSPOT_TYPE_KEY, false);

        //Show Close Wifi Hotspot Intro
        isShowCloseWifiHotspotIntro = sharedPref.getBoolean(AppUtils.SHOW_INTRO_CLOSE_WIFI_HOTSPOT_TYPE_KEY, false);

        //Init admob or facebook
        initAds();

        //if first time run then show user guide
        if (isFirstTime()) {
            loadTutorial();
        }

        try {

            String package_name = MainAppActivity.this.getPackageName();
            Answers.getInstance().logCustom(new CustomEvent("PackageName")
                    .putCustomAttribute("PackageName", package_name));

            Bundle params = new Bundle();
            params.putString("package_name", package_name);
            mFirebaseAnalytics.logEvent("PackageName", params);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

//        final View actionA = findViewById(R.id.action_a);
//        final View actionB = findViewById(R.id.action_b);

//        FloatingActionButton actionC = new FloatingActionButton(getBaseContext());
//        actionC.setTitle("Hide/Show Action above");
//        actionC.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                actionB.setVisibility(actionB.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
//            }
//        });

        //Get Network information
        getNetworkInformation();

        //Free Hotspot
        mHotUtil = new wifiHotSpots(getApplicationContext());
        mWifiStatus = new WifiStatus(getApplicationContext());
    }


    @Override
    protected void onStart() {
        super.onStart();
        RequestAdsToDisplay();

        //Check version update
        checkVersionUpdate();
    }

    @Override
    public void onBackPressed() {

        if(!canExitAppWithOutRateApp){
            rateApp();
            return;
        }

        super.onBackPressed();
    }

    private void getNetworkInformation() {

        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getExternal_IP() == null) {
            getExternal_IP();
        }


        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
            getNetworkInfo();
        }
    }


    private void getNetworkInfo(){
        String get_networkinfo_url = "http://ip-api.com/json/";
        //Volley
        GsonRequest reqNetworkInfo = ServiceAPI.getNetworkInfo(get_networkinfo_url,new Response.Listener<NetworkInformation>() {
            @Override
            public void onResponse(NetworkInformation response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setNetworkInformation(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqNetworkInfo.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqNetworkInfo, TAG);
    }

    private void getExternal_IP(){
        String get_external_ip_url = "https://api.ipify.org?format=json";
        //Volley
        GsonRequest reqExternalIp = ServiceAPI.getExternalIP(get_external_ip_url,new Response.Listener<External_IP>() {
            @Override
            public void onResponse(External_IP response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setExternal_IP(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqExternalIp.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqExternalIp, TAG);
    }

    //Check version update on play store
    private void checkVersionUpdate(){
        try {
            if (!MainAppActivity.this.isFinishing()) {
                UpdateChecker checker = new UpdateChecker(MainAppActivity.this); // If you are in a Activity or a FragmentActivity
                checker.setNoticeIcon(R.mipmap.ic_launcher);
//                checker.setNotice(Notice.NOTIFICATION);
                checker.setSuccessfulChecksRequired(3);
                checker.start();
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private void RequestAdsToDisplay() {

        if(mRequestAdsConfig){
            return;
        }

        String adsconfigs_url = this.getResources().getString(R.string.ads_configs_url);
        //Volley
        GsonRequest req = ServiceAPI.getAdsRequest(adsconfigs_url,new Response.Listener<AppConfig>() {
            @Override
            public void onResponse(AppConfig response) {
                Log.d("NTL", "RequestAdsToDisplay = onResponse", null);

                try {

                    String packagename = MainAppActivity.this.getPackageName();
                    if(!packagename.equalsIgnoreCase(response.getData().getPackagename())){
                        finish();
                    }

                    AppUtils.appConfig = response;
                    AppUtils.defaultads = response.getData().getDefaultads();
                    AppUtils.interstitial_count_maximum = response.getData().getAds_interstitial_count();
                    AppUtils.ads_interstitial_show_all = response.getData().isAds_interstitial_show_all();

                    if(AppUtils.defaultads.equalsIgnoreCase("admob")) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(AppUtils.ADS_TYPE_KEY, AppUtils.ADMOB_ADS_TYPE);
                        editor.apply();

                    }else if(AppUtils.defaultads.equalsIgnoreCase("facebook")) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(AppUtils.ADS_TYPE_KEY, AppUtils.FB_ADS_TYPE);
                        editor.apply();
                    }


                    if(AppUtils.ads_interstitial_show_all){
                        //Init all Ad InterstitialAd
                        AppUtils.getInstance().fullAdmobAdsBannerInit(MainAppActivity.this);
                        //Interstitial
                        mFBInterstitialAd = AppUtils.getInstance().FBfullBannerInit(MainAppActivity.this);
                    }

                    mRequestAdsConfig = true;

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }


//                int size = response.getData().getAds().size();
//                for(int i=0; i < size; i++){
//                    String source = response.getData().getAds().get(i).getSource();
//                    if(!source.equals(AppUtils.defaultads)){
//                        continue;
//                    }
//                    AppUtils.source = source;
//                    AppUtils.ads_banner_id = response.getData().getAds().get(i).getAds_banner_id();
//                    AppUtils.ads_interstitial_id = response.getData().getAds().get(i).getAds_interstitial_id();
//                    AppUtils.ads_native_id = response.getData().getAds().get(i).getAds_native_id();
//                    break;
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("NTL", "RequestAdsToDisplay = onErrorResponse", null);
            }
        });

        req.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(req, TAG);

    }

//    private void fetchRemoteConfig() {
//        mFirebaseRemoteConfig.fetch().addOnCompleteListener(
//                new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            mFirebaseRemoteConfig.activateFetched();
//                        }
//                        initConfig();
//                    }
//                }
//        );
//    }
//
//    private void initConfig() {
//        long ads_source = mFirebaseRemoteConfig.getLong("ads_source");
//        long ads_banner_id = mFirebaseRemoteConfig.getLong("ads_banner_id");
//        long ads_interstitial_id = mFirebaseRemoteConfig.getLong("ads_interstitial_id");
//        long ads_native_id = mFirebaseRemoteConfig.getLong("ads_native_id");
//
//        Log.d(TAG, "ads_source = " + ads_source, null);
//        Log.d(TAG, "ads_banner_id = " + ads_banner_id, null);
//        Log.d(TAG, "ads_interstitial_id = " + ads_interstitial_id, null);
//
//        if (mFirebaseRemoteConfig.getBoolean("is_ads_enable")) {
//            Log.d(TAG, "is_ads_enable = true", null);
//        } else {
//            Log.d(TAG, "is_ads_enable = false", null);
//        }
//    }



    private com.facebook.ads.InterstitialAd mFBInterstitialAd;
    private void initAds(){
//        AdSettings.addTestDevice("2ad1152013a23ee9cd88586b990e02bf");


        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

            //Interstitial
            AppUtils.getInstance().fullAdmobAdsBannerInit(this);


        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

            //Interstitial
            mFBInterstitialAd = AppUtils.getInstance().FBfullBannerInit(this);
        }

    }

    @Override
    protected void onDestroy() {
        if (mFBInterstitialAd != null) {
            mFBInterstitialAd.destroy();
        }
        mWifiHotspotEnable = false;
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_refresh:

                Answers.getInstance().logContentView(new ContentViewEvent()
                        .putContentName("Refresh WiFi"));

                if(wifiConnectFragment != null){
                    wifiConnectFragment.refreshWifiHotspot();
                }
                return true;
            case R.id.speedtest_history:

                try {
                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("History Results"));

                    if(AppUtils.ads_interstitial_show_all) {

                        if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                            AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
                                @Override
                                public void onAdClosed() {

                                    Intent intent = new Intent(MainAppActivity.this, SpeedTestHistoryActivity.class);
                                    intent.putExtra(MainAppActivity.COLOR_MESSAGE, currentColor);
                                    startActivity(intent);
                                }
                            });
                        } else {

                            intent = new Intent(MainAppActivity.this, SpeedTestHistoryActivity.class);
                            intent.putExtra(MainAppActivity.COLOR_MESSAGE, currentColor);
                            startActivity(intent);

                        }

                    }else {

                        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                            AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
                                @Override
                                public void onAdClosed() {

                                    Intent intent = new Intent(MainAppActivity.this, SpeedTestHistoryActivity.class);
                                    intent.putExtra(MainAppActivity.COLOR_MESSAGE, currentColor);
                                    startActivity(intent);
                                }
                            });
                        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                            AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
                                @Override
                                public void onAdClosed() {

                                    Intent intent = new Intent(MainAppActivity.this, SpeedTestHistoryActivity.class);
                                    intent.putExtra(MainAppActivity.COLOR_MESSAGE, currentColor);
                                    startActivity(intent);
                                }
                            });
                        } else {

                            intent = new Intent(MainAppActivity.this, SpeedTestHistoryActivity.class);
                            intent.putExtra(MainAppActivity.COLOR_MESSAGE, currentColor);
                            startActivity(intent);
                        }
                    }

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }



                return true;

            case R.id.wifi_hotspot:

                Answers.getInstance().logContentView(new ContentViewEvent()
                        .putContentName("Wifi Hotspot"));

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (!Settings.System.canWrite(MainAppActivity.this)) {
//                        MainAppActivityPermissionsDispatcher.inviteFriendWithPermissionCheck(MainAppActivity.this, mHotUtil);
//                        break;
//                    }
//                }
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (!Settings.System.canWrite(getApplicationContext())) {
//                        intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
//                        startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
//                        break;
//                    }
//                }

                if(AppUtils.ads_interstitial_show_all) {

                    if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                        AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
                            @Override
                            public void onAdClosed() {

                                inviteFriend(mHotUtil);
                            }
                        });

                    } else {

                        inviteFriend(mHotUtil);
                    }

                }else {

                    if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                        AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
                            @Override
                            public void onAdClosed() {

                                inviteFriend(mHotUtil);
                            }
                        });
                    } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                        AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
                            @Override
                            public void onAdClosed() {

                                inviteFriend(mHotUtil);
                            }
                        });
                    } else {

                        inviteFriend(mHotUtil);
                    }

                }

                return true;

            case R.id.action_rateus:

                AppUtils.getInstance().openPlaystore(this);

                Answers.getInstance().logContentView(new ContentViewEvent()
                        .putContentName("Action Rate Us"));


                return true;


            case R.id.action_aboutus:

                intent = new Intent(this, AboutActivity.class);
                intent.putExtra(MainAppActivity.COLOR_MESSAGE, currentColor);
                startActivity(intent);

                Answers.getInstance().logContentView(new ContentViewEvent()
                        .putContentName("About"));

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static int requestCode_ACTION_MANAGE_WRITE_SETTINGS = 200;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == requestCode_ACTION_MANAGE_WRITE_SETTINGS){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())) {
                    retryOpenWifiHotspot();
                } else {
                    inviteFriend(mHotUtil);
                }
            }
        }
    }

    private void retryOpenWifiHotspot(){
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Unable to Open Free Hotspot", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        inviteFriend(mHotUtil);
                    }
                });

        snackbar.show();
    }
    //    @NeedsPermission(Manifest.permission.WRITE_SETTINGS)
    public void inviteFriend(wifiHotSpots hotutil)
    {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
                    return;
                }
            }

            if(hotutil != null){

//            Toast.makeText(MainAppActivity.this,"Enable Free WiFi Hotspot",Toast.LENGTH_LONG).show();

                //Start Hotspot
                hotutil.setAndStartHotSpotCheckAndroidVersion(true, "FreeHotspot", "thankyou");
                mWifiHotspotEnable = true;

                final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                        .setTitleText("Loading");
                pDialog.show();
                pDialog.setCancelable(false);
                new CountDownTimer(800 * 2, 800) {
                    public void onTick(long millisUntilFinished) {
                        // you can change the progress bar color by ProgressHelper every 800 millis
                        i++;
                        switch (i){
                            case 0:
                                pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.blue_btn_bg_color));
                                break;
                            case 1:
                                pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_50));
                                break;
                            case 2:
                                pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                                break;
                            case 3:
                                pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_20));
                                break;
                            case 4:
                                pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_blue_grey_80));
                                break;
                            case 5:
                                pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.warning_stroke_color));
                                break;
                            case 6:
                                pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                                break;
                        }
                    }

                    public void onFinish() {
                        i = -1;
                        pDialog.setTitleText("Enable Success!")
                                .setConfirmText("OK")
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        if(wifiConnectFragment != null){
                            wifiConnectFragment.update_wifi_hotspot_enable_status();
                        }

                        if(!isShowCloseWifiHotspotIntro) {
//                            ImageView mExit_hotspot = (ImageView) findViewById(R.id.exit_hotspot);
//                            showIntroCloseWifiHotspot(mExit_hotspot);
                        }
                    }
                }.start();

            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

    }

    private void changeColor(int newColor) {
        tabs.setBackgroundColor(newColor);

//        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
//        viewPagerTab.setBackgroundColor(newColor);

        // change ActionBar color just if an ActionBar is available
        Drawable colorDrawable = new ColorDrawable(newColor);
        Drawable bottomDrawable = new ColorDrawable(ContextCompat.getColor(getBaseContext(), android.R.color.transparent));
        LayerDrawable ld = new LayerDrawable(new Drawable[]{colorDrawable, bottomDrawable});
        if (oldBackground == null) {
            getSupportActionBar().setBackgroundDrawable(ld);
        } else {
            TransitionDrawable td = new TransitionDrawable(new Drawable[]{oldBackground, ld});
            getSupportActionBar().setBackgroundDrawable(td);
            td.startTransition(200);
        }

        oldBackground = ld;
        currentColor = newColor;
    }

    public void onColorClicked(View v) {
        int color = Color.parseColor(v.getTag().toString());
        changeColor(color);
    }

    private void rateApp(){

        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
//                .session(3)
                .threshold(3)
                .titlebarcolor(currentColor)
                .title("Enjoy Free WiFi ?" +
                        "\n\nRecommend this app to others by leaving us a review in the Play Store !" +
                        "\n\nPlease give us 5 star" +
                        "\n\nSelect star below to rate now !")
//                .titleTextColor(R.color.black)
                .positiveButtonText("Later")
//                .negativeButtonText("Never")
                .positiveButtonTextColor(R.color.accent)
//                .negativeButtonTextColor(R.color.grey_500)
                .formTitle("Send Feedback")
                .formHint("Tell us where we can improve")
                .formSubmitText("Send")
                .formCancelText("Dismiss")
//                .ratingBarColor(R.color.black)
//                .positiveButtonBackgroundColor(R.drawable.button_selector_positive)
//                .negativeButtonBackgroundColor(R.drawable.button_selector_negative)
                .onThresholdCleared(new RatingDialog.Builder.RatingThresholdClearedListener() {
                    @Override
                    public void onThresholdCleared(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {
                        //do something

                        ratingDialog.openPlaystore(MainAppActivity.this);

//                        Answers.getInstance().logCustom(new CustomEvent("Rate")
//                                .putCustomAttribute("Star Rating", rating + ""));

                        markRateAppComplete();

                        ratingDialog.dismiss();

                    }
                })
//                .onThresholdFailed(new RatingDialog.Builder.RatingThresholdFailedListener() {
//                    @Override
//                    public void onThresholdFailed(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {
//                        //do something
//                        ratingDialog.dismiss();
//                    }
//                })
                .onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared) {
                        //do something
                        Answers.getInstance().logCustom(new CustomEvent("Rate")
                                .putCustomAttribute("Star Rating", rating + ""));

                    }
                })
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {
                        Answers.getInstance().logCustom(new CustomEvent("Rate")
                                .putCustomAttribute("Feedback", feedback));

                        markRateAppComplete();
                        showDialogThankyou();
                    }
                })
                .onRatingDialogClick(new RatingDialog.Builder.RatingDialogOnClickListener() {
                    @Override
                    public void onPositiveButtonSelected() {
                        canExitAppWithOutRateApp = true;
                    }

                    @Override
                    public void onNegativeButtonSelected() {

                    }

                    @Override
                    public void onDismissButtonSelected() {

                    }
                })
                .build();

        ratingDialog.show();
    }

    private void showDialogThankyou(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainAppActivity.this);
        alertDialogBuilder.setTitle("Thank you !");
        alertDialogBuilder.setMessage("Thank you for your support and give us a feedback we promise to improve as soon as possible.");
        alertDialogBuilder.setPositiveButton("Dismiss",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        alertDialogBuilder.show();
    }

    private void markRateAppComplete(){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(AppUtils.RATING_APP_TYPE_KEY, true);
        editor.apply();

        canExitAppWithOutRateApp = true;
    }

    private void showIntroSpeedtestHistory(View view){
        if(view == null) return;

        if(!isShowSpeedTestIntro) {

            new MaterialIntroView.Builder(this)
//                    .enableDotAnimation(true)
                    .setTargetPadding(40)
                    .enableIcon(false)
                    .setFocusGravity(FocusGravity.CENTER)
                    .setFocusType(Focus.MINIMUM)
                    .setDelayMillis(500)
                    .enableFadeAnimation(true)
                    .performClick(true)
                    .setInfoText("Hi There! You can now see all your results history.")
                    .setShape(ShapeType.CIRCLE)
                    .setTarget(view)
                    .setUsageId("intro_speed_history") //THIS SHOULD BE UNIQUE ID
                    .dismissOnTouch(true)
                    .show();

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_HISTORY_TYPE_KEY, true);
            editor.apply();

            isShowSpeedTestIntro = true;
        }
    }

    private void showIntroWifiHotspot(View view){
        if(view == null) return;

        if(!isShowWifiHotspotIntro) {

            new MaterialIntroView.Builder(this)
//                    .enableDotAnimation(true)
                    .setTargetPadding(40)
                    .enableIcon(false)
                    .setFocusGravity(FocusGravity.CENTER)
                    .setFocusType(Focus.MINIMUM)
                    .setDelayMillis(500)
                    .enableFadeAnimation(true)
                    .performClick(true)
                    .setInfoText("Hi There! You can now share internet to your friend.")
                    .setShape(ShapeType.CIRCLE)
                    .setTarget(view)
                    .setUsageId("intro_wifi_hotspot") //THIS SHOULD BE UNIQUE ID
                    .dismissOnTouch(true)
                    .show();

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(AppUtils.SHOW_INTRO_WIFI_HOTSPOT_TYPE_KEY, true);
            editor.apply();

            isShowWifiHotspotIntro = true;
        }
    }

    private void showIntroCloseWifiHotspot(View view){
        if(view == null) return;

        if(!isShowCloseWifiHotspotIntro) {

            new MaterialIntroView.Builder(this)
//                    .enableDotAnimation(true)
                    .setTargetPadding(40)
                    .enableIcon(false)
                    .setFocusGravity(FocusGravity.CENTER)
                    .setFocusType(Focus.MINIMUM)
                    .setDelayMillis(500)
                    .enableFadeAnimation(true)
                    .performClick(true)
                    .setInfoText("Hi There! You can close free hotspot right here.")
                    .setShape(ShapeType.CIRCLE)
                    .setTarget(view)
                    .setUsageId("intro_close_wifi_hotspot") //THIS SHOULD BE UNIQUE ID
                    .dismissOnTouch(true)
                    .show();

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(AppUtils.SHOW_INTRO_CLOSE_WIFI_HOTSPOT_TYPE_KEY, true);
            editor.apply();

            isShowCloseWifiHotspotIntro = true;
        }
    }

    private boolean isFirstTime() {
        final String ranBeforeKey = "RanBefore";
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean(ranBeforeKey, false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(ranBeforeKey, true);
            editor.apply();
        }
        return !ranBefore;
    }

    public void loadTutorial() {

        try {
            Intent intent = new Intent(MainAppActivity.this, IntroActivity.class);
            startActivity(intent);
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }
}