package wimosalsafifreewifi.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.thefinestartist.Base;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import io.github.dreierf.materialintroscreen.MaterialIntroActivity;
import io.github.dreierf.materialintroscreen.MessageButtonBehaviour;
import io.github.dreierf.materialintroscreen.SlideFragmentBuilder;
import io.github.dreierf.materialintroscreen.animations.IViewTranslation;

public class IntroPermissinoCheck extends MaterialIntroActivity {
    private static final int SEND_SMS_PERMISSION_REQUEST_CODE = 1000;
    private static final String TAG = "TEST";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Base.initialize(this);
        enableLastSlideAlphaExitTransition(true);

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

        if (!Settings.System.canWrite(this)) {

            addSlide(new CustomSlide());

        }
//        addSlide(new CustomSlide());
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            addSlide(new SlideFragmentBuilder()
                            .backgroundColor(R.color.third_slide_background)
                            .buttonsColor(R.color.third_slide_buttons)
                            .neededPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
                            .image(R.drawable.allow_location)
                            .title("Access Device Location")
                            .description("We needs to access location permission")
                            .build(),

                    new MessageButtonBehaviour(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showMessage("Try us!");
                        }
                    }, "ACCEPTED"));

        }

//        addSlide(new SlideFragmentBuilder()
//                        .backgroundColor(R.color.third_slide_background)
//                        .buttonsColor(R.color.third_slide_buttons)
//                        .neededPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION})
//                        .image(R.drawable.allow_location)
//                        .title("Access Device Location")
//                        .description("We needs to access location permission")
//                        .build(),
//
//                new MessageButtonBehaviour(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showMessage("Try us!");
//                    }
//                }, "ACCEPTED"));
    }

    @Override
    public void onFinish() {
        super.onFinish();
//        Toast.makeText(this, "Try this library in your project! :)", Toast.LENGTH_SHORT).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        try {
//            Toast.makeText(this, "onRequestPermissionsResult", Toast.LENGTH_LONG).show();

            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.

                Log.d("", "If request is cancelled, the result arrays are empty.");
                finish();
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Log.d("", "permission denied, boo! Disable the");

                boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION);
                if (!showRationale) {
                    //execute when 'never Ask Again' tick and permission dialog not show
                    Log.d("", "showRationale = " + showRationale);

                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);

                    Toast.makeText(this, "Please grant permissions on your device settings", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Log.d("", "showRationale = " + showRationale);

                }

            }
        } catch (Exception ignored){

        }

    }

//    PermissionUtils Detect “Dont’ ask again” option
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    public static boolean neverAskAgainSelected(final Activity activity, final String permission) {
//        final boolean prevShouldShowStatus = getRatinaleDisplayStatus(activity,permission);
//        final boolean currShouldShowStatus = activity.shouldShowRequestPermissionRationale(permission);
//        return prevShouldShowStatus != currShouldShowStatus;
//    }
//
//    public static void setShouldShowStatus(final Context context, final String permission) {
//        SharedPreferences genPrefs = context.getSharedPreferences("GENERIC_PREFERENCES", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = genPrefs.edit();
//        editor.putBoolean(permission, true);
//        editor.commit();
//    }
//    public static boolean getRatinaleDisplayStatus(final Context context, final String permission) {
//        SharedPreferences genPrefs =     context.getSharedPreferences("GENERIC_PREFERENCES", Context.MODE_PRIVATE);
//        return genPrefs.getBoolean(permission, false);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        //Note: I have placed this code in onResume for demostration purpose. Be careful when you use it in
//        // production code
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager
//                .PERMISSION_GRANTED) {
//            //You can show permission rationale if shouldShowRequestPermissionRationale() returns true.
//            //I will skip it for this demo
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (neverAskAgainSelected(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//                    displayNeverAskAgainDialog();
//                } else {
//                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                            SEND_SMS_PERMISSION_REQUEST_CODE);
//                }
//            }
//
//        }
//    }
//
//    private void displayNeverAskAgainDialog() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage("We need to send SMS for performing necessary task. Please permit the permission through "
//                + "Settings screen.\n\nSelect Permissions -> Enable permission");
//        builder.setCancelable(false);
//        builder.setPositiveButton("Permit Manually", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                Intent intent = new Intent();
//                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                Uri uri = Uri.fromParts("package", getPackageName(), null);
//                intent.setData(uri);
//                startActivity(intent);
//            }
//        });
//        builder.setNegativeButton("Cancel", null);
//        builder.show();
//    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
//            grantResults) {
//        if (SEND_SMS_PERMISSION_REQUEST_CODE == requestCode) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Log.i(TAG, "Permission granted successfully");
//                Toast.makeText(this, "Permission granted successfully", Toast.LENGTH_LONG).show();
//            } else {
//                setShouldShowStatus(this, Manifest.permission.ACCESS_FINE_LOCATION);
//            }
//        }
//    }
}