package wimosalsafifreewifi.main;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;
import wimosalsafispeedtest.fragment.MainFreeWiFiFragment;
import wimosalsafispeedtest.fragment.SpeedDetailFragment;
import wimosalsafispeedtest.fragment.SpeedTestHistoryFragment;
import wimosalsafispeedtest.fragment.SpeedTestMiniFragment;
import wimosalsafiwifimap.fragment.SpeedMapFragment;

/**
 * Created by GIGAMOLE on 28.03.2016.
 */
public class HorizontalNtbActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horizontal_ntb);

        final ViewPager pager = (ViewPager) findViewById(R.id.vp_horizontal_ntb);
        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
                @Override
                public int getCount() {
                    return 5;
                }

                // Returns the fragment to display for that page
                @Override
                public Fragment getItem(int position) {
                    switch (position) {
                        case 0: // Fragment # 0 - This will show FirstFragment

                            return new SpeedMapFragment();
                        case 1: // Fragment # 1 - This will show SecondFragment

                            return new MainFreeWiFiFragment();
                        case 2: // Fragment # 2 - This will show ThirdFragment

                            return new SpeedDetailFragment();
                        case 3: // Fragment # 3 - This will show FourthFragment

                            return new SpeedTestHistoryFragment();
                        case 4: // Fragment # 4 - This will show FifthFragment

                            return new SpeedTestMiniFragment();
                        default:
                            return new SpeedMapFragment();
                    }
                }

                // Returns the page title for the top indicator
                @Override
                public CharSequence getPageTitle(int position) {
                    return "Page" + position;
                }


            });

            final String[] colors = getResources().getStringArray(R.array.default_preview);

            final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
            final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
            models.add(
                    new NavigationTabBar.Model.Builder(
                            getResources().getDrawable(R.drawable.ic_first),
                            Color.parseColor(colors[0]))
                            .selectedIcon(getResources().getDrawable(R.drawable.ic_sixth))
                            .title("Heart")
                            .badgeTitle("NTB")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            getResources().getDrawable(R.drawable.ic_second),
                            Color.parseColor(colors[1]))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                            .title("Cup")
                            .badgeTitle("with")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            getResources().getDrawable(R.drawable.ic_third),
                            Color.parseColor(colors[2]))
                            .selectedIcon(getResources().getDrawable(R.drawable.ic_seventh))
                            .title("Diploma")
                            .badgeTitle("state")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            getResources().getDrawable(R.drawable.ic_fourth),
                            Color.parseColor(colors[3]))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                            .title("Flag")
                            .badgeTitle("icon")
                            .build()
            );
            models.add(
                    new NavigationTabBar.Model.Builder(
                            getResources().getDrawable(R.drawable.ic_fifth),
                            Color.parseColor(colors[4]))
                            .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                            .title("Medal")
                            .badgeTitle("777")
                            .build()
            );

            navigationTabBar.setModels(models);
            navigationTabBar.setViewPager(pager, 0);
            navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(final int position) {
                    navigationTabBar.getModels().get(position).hideBadge();
                }

                @Override
                public void onPageScrollStateChanged(final int state) {

                }
            });

            navigationTabBar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                        final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                        navigationTabBar.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                model.showBadge();
                            }
                        }, i * 100);
                    }
                }
            }, 500);

    }


}