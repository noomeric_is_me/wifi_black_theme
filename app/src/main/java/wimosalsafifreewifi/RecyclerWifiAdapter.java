package wimosalsafifreewifi;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import wimosalsafifreewifi.fragment.WifiConnectFragment_Recycler_v1;
import wimosalsafifreewifi.wificonnector.ConfigurationSecurities;
import utils.AppUtils;

public class RecyclerWifiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private Context context;
    private List<ScanResult> listData;
    private List<String> arrayListNames;
    private WifiManager wifiManager;
    private final ArrayList<Integer> channelsFrequency = new ArrayList<Integer>(
            Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
                    2452, 2457, 2462, 2467, 2472, 2484));

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public RecyclerWifiAdapter(Context context){
        this.context = context;
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    public void setListener(OnItemClickListener listener){
        mListener = listener;
    }

    public void add(List<ScanResult> listWifiData) {
        listData = listWifiData;
    }

//    @Override
//    public int getItemViewType(int position) {
//        switch (position) {
//            case 0:
//                return TYPE_HEADER;
//            default:
//                return TYPE_CELL;
//        }
//    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

//        switch (viewType) {
//            case TYPE_HEADER: {
//                view = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.list_item_card_big, parent, false);
//                return new RecyclerView.ViewHolder(view) {
//                };
//            }
//            case TYPE_CELL: {
//                view = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.list_item_card_small, parent, false);
//                return new RecyclerView.ViewHolder(view) {
//                };
//            }
//        }
//        return null;


//        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_wifi_row, parent, false);
//        return new RecyclerView.ViewHolder(view) {
//        };

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_wifi_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.setinfo(position);
    }

//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }

    @Override
    public int getItemCount() {
        return listData == null ? 0 : listData.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView signalLevel;
        private ProgressBar signalStrength;
        private TextView address;
        private ConfigurationSecurities conf;
        private ImageView lock;
        private TextView secureType;
        private TextView name;
        private TextView channel;
        private WifiInfo info;
        private TextView connected;
        private TextView bbssid;
        private View mView;

        ViewHolder(View v) {
            super(v);
            mView = v;
            signalLevel = (ImageView) v.findViewById(R.id.image_signal);
            signalStrength = (ProgressBar) v.findViewById(R.id.signal_bar);
            address = (TextView) v.findViewById(R.id.text_signal);
            conf = new ConfigurationSecurities();
            lock = (ImageView) v.findViewById(R.id.image_lock);
            secureType = (TextView) v.findViewById(R.id.text_secure);
            name = (TextView) v.findViewById(R.id.text_name);
            channel = (TextView) v.findViewById(R.id.text_channel);
            info = wifiManager.getConnectionInfo();
            connected = (TextView) v.findViewById(R.id.text_connect);
            bbssid = (TextView) v.findViewById(R.id.text_bbssid);
            mView.setOnClickListener(this);
        }

        void setinfo(int position){
            ScanResult scanresult = listData.get(position);

            int strength = scanresult.level;
            int level = WifiManager.calculateSignalLevel(strength, 4);
            switch (level+1){
                case 1:
                    signalLevel.setImageResource(R.drawable.signal_wifi1);
                    break;
                case 2:
                    signalLevel.setImageResource(R.drawable.signal_wifi2);
                    break;
                case 3:
                    signalLevel.setImageResource(R.drawable.signal_wifi3);
                    break;
                case 4:
                    signalLevel.setImageResource(R.drawable.signal_wifi4);
                    break;
            }

            int color = AppUtils.getInstance().getRandomColor(0);

            signalStrength.setMax(100);
            signalStrength.setProgress(100 + strength);
            signalStrength.getProgressDrawable().setColorFilter(
                    color, android.graphics.PorterDuff.Mode.SRC_IN);

            address.setText(scanresult.level + " dBm");
//            address.setBackgroundColor(color);
            address.setTextColor(color);

            String lockType = conf.getDisplaySecirityString(scanresult);

            if(!lockType.equals("OPEN")){
                lock.setImageResource(R.mipmap.lock);
            }else {
                lock.setImageResource(R.mipmap.lock_unlock);
            }

            secureType.setText(lockType);
            Log.d("NTL:", "lockType = " + lockType);
            name.setText(scanresult.SSID.trim());
            int frequency = scanresult.frequency;
            int channelLevel = channelsFrequency.indexOf(Integer.valueOf(frequency));
            channel.setText( context.getString(R.string.text_channel) + " " + channelLevel
                    +" : "+ frequency + " MHz ");
            bbssid.setText(scanresult.BSSID);

            if(info.getBSSID() != null){
                String BSSID = info.getBSSID();
                String SSID = info.getSSID();
                String listBSSID = scanresult.BSSID;
                String listSSID = scanresult.SSID;

                if(info.getBSSID().equals(scanresult.BSSID)) {
                    connected.setText(WifiConnectFragment_Recycler_v1.WIFI_STATE_CONNECT == 2 ?
                            context.getString(R.string.text_connected) :
                            context.getString(R.string.text_connecting));
                    connected.setVisibility(View.VISIBLE);
                }else {
                    connected.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onClick(View view) {
//            mListener.onClick(view, getAdapterPosition());
            mListener.onItemClick(view, getAdapterPosition());
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                arrayListNames = (List<String>) results.values;
                //Log.d("debugging","arrayListNames" + arrayListNames);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<String> FilteredArrayNames = new ArrayList<String>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < listData.size(); i++) {
                    String dataNames = listData.get(i).SSID;
                    if (dataNames.toLowerCase().startsWith(constraint.toString()))  {
                        FilteredArrayNames.add(dataNames);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;

                return results;
            }
        };

        return filter;
    }
}
