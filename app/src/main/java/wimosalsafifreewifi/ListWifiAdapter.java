package wimosalsafifreewifi;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import wimosalsafifreewifi.fragment.WifiConnectFragment;
import wimosalsafifreewifi.wificonnector.ConfigurationSecurities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import utils.AppUtils;

public class ListWifiAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<ScanResult> listData;
    private List<String> arrayListNames;
    private WifiManager wifiManager;
    private final ArrayList<Integer> channelsFrequency = new ArrayList<Integer>(
            Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
                    2452, 2457, 2462, 2467, 2472, 2484));

    public ListWifiAdapter(Context context){
        this.context = context;
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    public void add(List<ScanResult> listWifiData) {
        listData = listWifiData;
    }
    @Override
    public int getCount() {
        return listData == null ? 0 : listData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.list_wifi_row,parent,false);

        ImageView signalLevel = (ImageView) v.findViewById(R.id.image_signal);
        int strength = listData.get(position).level;
        int level = WifiManager.calculateSignalLevel(strength, 4);
        switch (level+1){
            case 1:
                signalLevel.setImageResource(R.drawable.signal_wifi1);
                break;
            case 2:
                signalLevel.setImageResource(R.drawable.signal_wifi2);
                break;
            case 3:
                signalLevel.setImageResource(R.drawable.signal_wifi3);
                break;
            case 4:
                signalLevel.setImageResource(R.drawable.signal_wifi4);
                break;
        }

        int color = AppUtils.getInstance().getRandomColor(0);
        ProgressBar signalStrength = (ProgressBar) v.findViewById(R.id.signal_bar);
        signalStrength.setMax(100);
        signalStrength.setProgress(100 + strength);
//        signalStrength.setBackgroundColor(color);
        signalStrength.getProgressDrawable().setColorFilter(
                color, android.graphics.PorterDuff.Mode.SRC_IN);

        TextView address = (TextView) v.findViewById(R.id.text_signal);
        address.setText(listData.get(position).level + " dBm");
        address.setBackgroundColor(color);

        ConfigurationSecurities conf = new ConfigurationSecurities();
        String lockType = conf.getDisplaySecirityString(listData.get(position));

        ImageView lock = (ImageView) v.findViewById(R.id.image_lock);
        if(!lockType.equals("OPEN")){
            lock.setImageResource(R.mipmap.lock);
        }else {
            lock.setImageResource(R.mipmap.lock_unlock);
        }

        TextView secureType = (TextView) v.findViewById(R.id.text_secure);
        secureType.setText(lockType);

        TextView name = (TextView) v.findViewById(R.id.text_name);
        name.setText(listData.get(position).SSID.trim());

        TextView channel = (TextView) v.findViewById(R.id.text_channel);
        int frequency = listData.get(position).frequency;
        int channelLevel = channelsFrequency.indexOf(Integer.valueOf(frequency));
        channel.setText( context.getString(R.string.text_channel) + " " + channelLevel
                +" : "+ frequency + " MHz ");

        WifiInfo info = wifiManager.getConnectionInfo();
        if(info.getBSSID() != null){
            if(info.getBSSID().equals(listData.get(position).BSSID)){
                TextView connected = (TextView) v.findViewById(R.id.text_connect);
                connected.setText(WifiConnectFragment.WIFI_STATE_CONNECT == 2?
                        "  " + context.getString(R.string.text_connected):
                        "  " + context.getString(R.string.text_connecting));
            }
        }

        return v;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                arrayListNames = (List<String>) results.values;
                //Log.d("debugging","arrayListNames" + arrayListNames);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<String> FilteredArrayNames = new ArrayList<String>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < listData.size(); i++) {
                    String dataNames = listData.get(i).SSID;
                    if (dataNames.toLowerCase().startsWith(constraint.toString()))  {
                        FilteredArrayNames.add(dataNames);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;

                return results;
            }
        };

        return filter;
    }
}
