package wimosalsafifreewifi.fragment;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import wimosalsafifreewifi.RecyclerWifiAdapter;
import wimosalsafifreewifi.application.AppController;
import utils.AppUtils;

import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;

/**
 * Created by NTL on 1/6/2017 AD.
 */

public class WifiConnectFragment_Recycler_v1 extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {
    public static final String TAG = "debugging";

    public static int WIFI_STATE_CONNECT;

    //    private ToggleButton toggleWifi;
    private ImageView btnSort;
    //    private ImageView stateView;
    private WifiManager wifiManager;
    private WifiInfo wifiInfo;
    private RecyclerWifiAdapter recyclerWifiAdapter;
    private List<ScanResult> listWifiData = new ArrayList<>();
    private List<ScanResult> listSearch = new ArrayList<>();
    private int sort = 1;
    private Handler guiThread;
    private Runnable updateTask;
    private AutoCompleteTextView txt_search;
    private RecyclerView mRecyclerView;
    private View mRootView;
    private static final int ITEM_COUNT = 100;
    private PublisherAdView mPublisherAdView;
    private FrameLayout adContainerView;
    private AdView adtView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.activity_wifi, container,
                    false);

            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recyclerView);

            btnSort = (ImageView) mRootView.findViewById(R.id.sort_btn);

            recyclerWifiAdapter = new RecyclerWifiAdapter(getActivity());
            recyclerWifiAdapter.setListener(new RecyclerWifiAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {

                    try {
                        if(listWifiData == null || position >= listWifiData.size()){
                            return;
                        }
                        final ScanResult result = listWifiData.get(position);
                        launchWifiConnecter(getActivity(), result);
                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                }
            });

            wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            final List<Object> items = new ArrayList<>();

            for (int i = 0; i < ITEM_COUNT; ++i) {
                items.add(new Object());
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setHasFixedSize(true);

            //Use this now
            mRecyclerView.setAdapter(recyclerWifiAdapter);

            setSwitchSort();
            btnSort.setOnClickListener(this);

            innitThread();
            txt_search = (AutoCompleteTextView) mRootView.findViewById(R.id.search_box);
            txt_search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    queueUpdate(500);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            //Init admob or facebook
            initAds();
        }

        return  mRootView;
    }

    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            adContainerView = mRootView.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
//            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads

            RelativeLayout adViewContainer = (RelativeLayout) mRootView.findViewById(R.id.adViewContainer);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);

        }
    }

    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private void queueUpdate(long delayMillisecond) {
        guiThread.removeCallbacks(updateTask);
        // update data if no change in textSearch after time config
        // timer by = milliseconds
        guiThread.postDelayed(updateTask, delayMillisecond);
    }

    private void innitThread() {
        guiThread = new Handler();
        updateTask = new Runnable() {
            @Override
            public void run() {

                String word = txt_search.getText().toString().trim();
                if (word.isEmpty()) {
                    // if not change set listView first
                    recyclerWifiAdapter.add(listWifiData);
                    recyclerWifiAdapter.notifyDataSetChanged();
                } else {
                    // get data from webservice
                    getDataByKeywords(word);
                    // Show on list
                    listSearch = null;
                    // get data from webservice
                    listSearch = getDataByKeywords(word);

                    recyclerWifiAdapter.add(listSearch);
                    recyclerWifiAdapter.notifyDataSetChanged();
                }

            }
        };
    }

    public List<ScanResult> getDataByKeywords(String keyword) {
        ArrayList<ScanResult> listFilter = new ArrayList<ScanResult>();
        keyword = keyword.toUpperCase();
        for (int i = 0; i < listWifiData.size(); i++) {
            String contain = listWifiData.get(i).SSID.toUpperCase();
            if (contain.contains(keyword)) {
                listFilter.add(listWifiData.get(i));
            }
        }
        return listFilter;
    }

    private void setSwitchSort() {
        switch (sort) {
            case 1:
                btnSort.setImageResource(R.drawable.sort_signal);
                break;
            case 2:
                btnSort.setImageResource(R.drawable.sort_alpha);
                break;
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        getActivity().registerReceiver(receiver, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
        getActivity().registerReceiver(receiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
        wifiManager.startScan();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    public void refreshWifiHotspot(){

        try {
            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
                    @Override
                    public void onAdClosed() {
                        refreshWifi_DoAfterAdClose();
                    }
                });
            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
                    @Override
                    public void onAdClosed() {
                        refreshWifi_DoAfterAdClose();
                    }
                });
            } else {
                refreshWifi_DoAfterAdClose();
            }

        } catch (Exception e) {
            Crashlytics.logException(e);
        }

    }

    private void refreshWifi_DoAfterAdClose(){
        Toast.makeText(AppController.getInstance().getAppContext(),"Refresh Wi-fi Success",Toast.LENGTH_LONG).show();
        if(listWifiData != null
                && recyclerWifiAdapter != null
                && wifiManager != null){

            listWifiData.clear();
            recyclerWifiAdapter.notifyDataSetChanged();
            wifiManager.startScan();
        }
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.sort_btn:
                switch (sort) {
                    case 1:
                        if(listWifiData != null
                                && recyclerWifiAdapter != null
                                && wifiManager != null){

                            listWifiData.clear();
                            recyclerWifiAdapter.notifyDataSetChanged();
                            wifiManager.startScan();
                            sort++;
                        }
                        break;
                    default:
                        if(listWifiData != null
                                && recyclerWifiAdapter != null
                                && wifiManager != null){

                            listWifiData.clear();
                            recyclerWifiAdapter.notifyDataSetChanged();
                            wifiManager.startScan();
                            sort = 1;
                        }
                        break;
                }
                setSwitchSort();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(listWifiData == null || position >= listWifiData.size()){
            return;
        }
        final ScanResult result = listWifiData.get(position);
        launchWifiConnecter(getActivity(), result);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                String action = intent.getAction();
                if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                    listWifiData = wifiManager.getScanResults();

                    if (sort == 1) {
                /* sorting of wifi provider based on level */
                        if(listWifiData != null){
                            Collections.sort(listWifiData, new Comparator<ScanResult>() {
                                @Override
                                public int compare(ScanResult lhs, ScanResult rhs) {
                                    return (lhs.level > rhs.level ? -1
                                            : (lhs.level == rhs.level ? 0 : 1));
                                }
                            });
                        }

                    } else if (sort == 2) {
                /* sorting of wifi provider based on name */
                        if(listWifiData != null){
                            Collections.sort(listWifiData, new Comparator<ScanResult>() {
                                @Override
                                public int compare(ScanResult lhs, ScanResult rhs) {
                                    int res = String.CASE_INSENSITIVE_ORDER.compare(lhs.SSID, rhs.SSID);
                                    if (res == 0) {
                                        res = lhs.SSID.compareTo(rhs.SSID);
                                    }
                                    return res;
                                }
                            });
                        }
                    }

                    wifiInfo = wifiManager.getConnectionInfo();
                    if (wifiInfo.getBSSID() != null) {
                        if(listWifiData != null){
                            for (int i = 0; i < listWifiData.size(); i++) {
                                if (wifiInfo.getBSSID().equals(listWifiData.get(i).BSSID)) {
                                    ScanResult itemToMove = listWifiData.get(i);
                                    listWifiData.remove(i);
                                    listWifiData.add(0, itemToMove);
                                }
                            }
                        }
                    }

                    guiThread.post(updateTask);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            wifiManager.startScan();
                        }
                    }, 5000);

                }

                if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
                    int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
                    switch (state) {
                        case WifiManager.WIFI_STATE_DISABLED:
                            break;
                        case WifiManager.WIFI_STATE_ENABLED:
                            wifiManager.startScan();
                            break;
                    }
                }

                if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {

                    NetworkInfo nwInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                    if (NetworkInfo.State.CONNECTING.equals(nwInfo.getState())) {
                        Log.d(TAG, "Wifi Connecting");
                        WIFI_STATE_CONNECT = 1;
                    } else if (NetworkInfo.State.CONNECTED.equals(nwInfo.getState())) {
                        Log.d(TAG, "Wifi Connected");
                        WIFI_STATE_CONNECT = 2;
                    }
                    recyclerWifiAdapter.notifyDataSetChanged();
                }
            } catch (Exception ignored) {
                Crashlytics.logException(ignored);
            }
        }
    };

    private void launchWifiConnecter(final Context activity, final ScanResult hotspot) {
        final Intent intent = new Intent("com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.action.CONNECT_OR_EDIT");
        intent.putExtra("com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.extra.HOTSPOT", hotspot);
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Wifi Connecter Library is not installed.

            if(mRecyclerView != null) {
                Snackbar.make(mRecyclerView, "There was an error please try again !", Snackbar.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "There was an error please try again !"
                        , Toast.LENGTH_LONG).show();

//                Toast.makeText(getApplicationContext(), "Wifi Connecter is not installed."
//                    , Toast.LENGTH_LONG).show();
            }
        }
    }



    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(Objects.requireNonNull(getActivity()));
        adtView.setAdUnitId(getResources().getString(R.string.admob_adaptive_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);

        return AdSize.getCurrentOrientationBannerAdSizeWithWidth(getActivity(), adWidth);
    }
}