package wimosalsafifreewifi.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import utils.AppUtils;

import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;

/**
 * Created by NTL on 1/6/2017 AD.
 */

public class ChartFragment extends Fragment {
    private String TAG = "debugging";

    private WifiManager wifiManager;
    private FrameLayout chartLayout;
    private XYMultipleSeriesRenderer renderer;
    private GraphicalView chartView;
    private View mRootView;
    private PublisherAdView mPublisherAdView;
    private FrameLayout adContainerView;
    private AdView adtView;

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R.layout.fragment_chart);
//        chartLayout = (FrameLayout) findViewById(com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R.id.chart);
//        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//        if(!wifiManager.isWifiEnabled()){
//            wifiManager.setWifiEnabled(true);
//        }
//
//        //Init admob or facebook
//        initAds();
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_chart, container,
                    false);

            chartLayout = (FrameLayout) mRootView.findViewById(R.id.chart);
            wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            try {
                if(!wifiManager.isWifiEnabled()){
                    wifiManager.setWifiEnabled(true);
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

            //Init admob or facebook
            initAds();
        }

        return  mRootView;
    }

    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);


            adContainerView = mRootView.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
//            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads

            RelativeLayout adViewContainer = (RelativeLayout) mRootView.findViewById(R.id.adViewContainer);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);
        }
    }

    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "scanReceiver received!");

//            chartView = ChartFactory.getBarChartView(getActivity(),
//                    getDataset(), renderer, BarChart.Type.STACKED);

            chartView = ChartFactory.getLineChartView(getActivity(),
                    getDataset(), renderer);

            chartView.repaint();
            chartLayout.removeAllViews();
            chartLayout.addView(chartView);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(wifiManager != null){
                            wifiManager.startScan();
                        }
                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                }
            }, 3000);
        }
    };

    private XYMultipleSeriesDataset getDataset() {

        List<ScanResult> results = wifiManager.getScanResults();

        if(results != null){
            Collections.sort(results, new Comparator<ScanResult>() {
                @Override
                public int compare(ScanResult lhs, ScanResult rhs) {
                    return (lhs.level > rhs.level ? -1
                            : (lhs.level == rhs.level ? 0 : 1));
                }
            });
        }

        renderer = getRenderer();
        XYSeries series = new XYSeries("Wifi Signal");
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

        if(results != null){
            for (int i = 0;i < results.size();i++) {
                series.add(i + 1, results.get(i).level + 100);
                renderer.addXTextLabel(i + 1, results.get(i).SSID);
            }
        }

        dataset.addSeries(series);

        return dataset;
    }

    private XYMultipleSeriesRenderer getRenderer() {
        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();

        mRenderer.setChartTitle("WiFi Signal");
        mRenderer.setYTitle("Signal Strength(%)");
        mRenderer.setBackgroundColor(Color.WHITE);
        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setAxesColor(Color.GRAY);
        mRenderer.setLabelsColor(Color.GRAY);
        mRenderer.setXLabelsColor(Color.GRAY);
        mRenderer.setYLabelsColor(0, Color.GRAY);
        mRenderer.setMarginsColor(Color.WHITE);
        mRenderer.setAxisTitleTextSize(20);
        mRenderer.setChartTitleTextSize(20);
        mRenderer.setLabelsTextSize(22);
        mRenderer.setXLabelsAngle(90);
        mRenderer.setXLabelsAlign(Paint.Align.LEFT);
        mRenderer.setMargins(new int[]{50, 50, 150, 20});
        mRenderer.setXAxisMin(0);
        mRenderer.setYAxisMax(100);
        mRenderer.setYAxisMin(0);
        mRenderer.setXLabels(0);
        mRenderer.setYLabels(10);
        mRenderer.setShowAxes(true);
        mRenderer.setShowLegend(false);
        mRenderer.setShowGridX(true);
        mRenderer.setShowGridY(false);
        mRenderer.setClickEnabled(false);
        mRenderer.setExternalZoomEnabled(false);
        mRenderer.setPanLimits(new double[]{0, 100, 0, 100});
        mRenderer.setZoomEnabled(true, false);
        mRenderer.setBarSpacing(0.200000001D);

        XYSeriesRenderer r = new XYSeriesRenderer();
        r.setDisplayChartValues(true);
        r.setChartValuesTextSize(20);
        r.setColor(AppUtils.getInstance().getRandomColor(0));
//        r.setColor(getResources().getColor(R.color.blue));
        r.setLineWidth(3.0F);
        r.setPointStyle(PointStyle.CIRCLE);

        mRenderer.addSeriesRenderer(r);

        return mRenderer;
    }





    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(Objects.requireNonNull(getActivity()));
        adtView.setAdUnitId(getResources().getString(R.string.admob_adaptive_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);

        return AdSize.getCurrentOrientationBannerAdSizeWithWidth(getActivity(), adWidth);
    }

}

