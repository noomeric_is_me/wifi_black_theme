package wimosalsafifreewifi.fragment;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.gms.ads.AdSize;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.NativeAd;

import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.mady.wifi.api.wifiHotSpots;
import com.thefinestartist.Base;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import utils.AppUtils;
import wimosalsafifreewifi.application.AppController;
import wimosalsafihotspot8.MagicActivity;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerFragment;


import static android.app.Activity.RESULT_OK;
import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;
import static com.thefinestartist.utils.content.ContextUtil.getPackageName;
import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;


/**
 * Created by intag pc on 2/12/2017.
 */

public class HotspotFragment extends FragmentPagerFragment {
    private Button bthotspot_on, bthotspot_off;
    public static final String TAG = HotspotFragment.class.getSimpleName();
    public static final String AP_NAME_WIFI_HOTSPOT_TYPE_KEY = "ap_name_wifi_hotspot_type_key";
    public static final String AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY = "ap_password_wifi_hotspot_type_key";
    private NestedScrollView mScrollView;

    public static HotspotFragment newInstance() {
        return new HotspotFragment();
    }

    private wifiHotSpots mHotUtil;
    public static boolean mWifiHotspotEnable;
    private SharedPreferences sharedPref;
    private String vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot;
    private ImageView vmkoomImageToggle;
    private EditText vmkoomPasswordInput, vmkoomSSIDInput;
    private CheckBox vmkoomShowPasswordCheckbox;
    private TextView vmkoomSettingSave, vmkoomSettingInformation;
    private Switch vmkoomSecuritySwitchToggle;
    ImageView mainbrush, cache, temp, residue, system;
    TextView maintext, cachetext, temptext, residuetext, systemtext;
    @SuppressLint("StaticFieldLeak")
    public static ImageView mainbutton;
    private Handler mHandler;
    private CardView hotspotconfig;
    private PublisherAdView mPublisherAdView;
    private int i = -1;
    int checkvar = 0;
    int alljunk;
    //Ad
    private AdView adView;
    private FrameLayout adContainerView;
    private AdView adtView;
    private InterstitialAd interstitialAd;
    private NativeAd nativeAd;
    private LinearLayout nativeAdContainer;
    private LinearLayout adViewLinearLayout;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    private View mRootView;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                mRootView= inflater.inflate(R.layout.hotspot_oreo_main, container, false);
//            } else {
//                mRootView= inflater.inflate(R.layout.hotspot_main, container, false);
//            }
//            mRootView= inflater.inflate(R.layout.hotspot_main, container, false);
            mRootView = inflater.inflate(R.layout.hotspot_main, container, false);
            mScrollView = mRootView.findViewById(R.id.scrollView);
            mHandler = new Handler();
            mHotUtil = new wifiHotSpots(getActivity());

//        ImageView iispeedtestii = (ImageView) mRootView.findViewById(R.id.speedtestii);
//        iispeedtestii.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View mRootView) {
//                final Intent intent;
//                intent = new Intent(getActivity(), IntroActivity.class);
//                startActivity(intent);
//            }
//        });
            hotspotconfig = this.mRootView.findViewById(R.id.confighotspot);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                hotspotconfig.setVisibility(View.GONE);
            } else {
                hotspotconfig.setVisibility(View.VISIBLE);
            }
            bthotspot_on = (Button) mRootView.findViewById(R.id.HotspotON);
            bthotspot_on.setVisibility(View.GONE);
            bthotspot_on.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    vmkoomShareWifiHotspot();
                }
            });

            bthotspot_off = (Button) mRootView.findViewById(R.id.HotspotOFF);
            bthotspot_off.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    vmkoomShareWifiHotspot();
                }
            });
            vmkoomImageToggle = mRootView.findViewById(R.id.vmkoom_image_toggle);
            vmkoomImageToggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    vmkoomShareWifiHotspot();
                }
//                @Override
//                public void onClick(View mRootView) {
//                    try {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
//                                .setTitleText("Are you sure?")
//                                .setContentText("Won't be able to recover this file!")
//                                .setCancelText("No,cancel plx!")
//                                .setConfirmText("Yes,delete it!")
//                                .setNeutralText("Setting")
//                                .showCancelButton(true)
//                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(SweetAlertDialog sDialog) {
//                                        vmkoomShareWifiHotspot();
//                                        sDialog.cancel();
//                                    }
//                                })
//                                .setNeutralClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(SweetAlertDialog sDialog) {
//                                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
//                                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
//                                        intent.setComponent(cn);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
//                                    }
//                                })
//                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(SweetAlertDialog sDialog) {
////                                        vmkoomShareWifiHotspot();
//                                        sDialog.cancel();
//                                    }
//                                })
//                                .show();
////                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
////                                .setTitleText("Not support on this Device!")
////                                .setContentText("We will update hotspot function to support Android 8, 9 as soon")
////                                .setConfirmText("Dismiss")
////                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
////                                    @Override
////                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
////                                        sweetAlertDialog.dismissWithAnimation();
////                                    }
////                                })
////                                .show();
//                    }else{
//                        vmkoomShareWifiHotspot();}
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M&&Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
//
//                        if (!Settings.System.canWrite(getActivity().getApplicationContext())) {
//                            AlertDialog.Builder alertadd = new AlertDialog.Builder(getActivity());
//                            LayoutInflater factory = LayoutInflater.from(getActivity());
//                            final View view = factory.inflate(R.layout.diaglog_settingpermission, null);
//                            alertadd.setView(view);
//
//                            alertadd.setNeutralButton("OK!", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dlg, int sumthin) {
//                                    settingPermission();
//                                }
//                            });
//
//                            alertadd.show();
//                        }
//                    }
//
//                    } catch (Exception ignored) {
//                        Crashlytics.logException(ignored);
//                    }
//                }
            });
            Base.initialize(Objects.requireNonNull(getActivity()));
            vmkoomConfigureHotspot();
            MobileAds.initialize(getActivity(), getResources().getString(R.string.admob_app_id));
            refreshAd();
            initAds();

        }
        return mRootView;

    }

    @Override
    public void onDestroy() {
        if (adViewfacebook != null) {
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private void settingPermissiondialog() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(Objects.requireNonNull(getActivity()).getApplicationContext())) {
                    AlertDialog.Builder alertadd = new AlertDialog.Builder(getActivity());
                    LayoutInflater factory = LayoutInflater.from(getActivity());
                    final View view = factory.inflate(R.layout.diaglog_settingpermission, null);
                    alertadd.setView(view);

                    alertadd.setNeutralButton("OK!", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dlg, int sumthin) {

                        }
                    });

                    alertadd.show();
                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    public void settingPermission() {
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 200);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private com.facebook.ads.AdView adViewfacebook;

    //    private NativeAdsManager manager;
//    private NativeAdScrollView nativeAdScrollView;
    private void initAds() {

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            adContainerView = mRootView.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
//            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

//            mPublisherAdView = (PublisherAdView) mRootView.findViewById(R.id.fluid_view);
//            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mRootView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(AppController.getInstance().getAppContext(), adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mRootView.findViewById(R.id.hotspot_native_ad_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeSmallInit(AppController.getInstance().getAppContext(), adNativeViewContainer);
        }
    }

    public void showInterstitial() {
        try {
            if (AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            } else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private void vmkoomConfigureHotspot() {
        sharedPref = Objects.requireNonNull(getActivity()).getPreferences(Context.MODE_PRIVATE);
        vmkoomNameWifiHotspot = sharedPref.getString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, "FreeWiFi");
        vmkoomPasswordWifiHotspot = sharedPref.getString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, "11111111");

        vmkoomSettingInformation = this.mRootView.findViewById(R.id.vmkoom_setting_information);
        vmkoomSettingInformation.setVisibility(View.GONE);

        vmkoomSSIDInput = this.mRootView.findViewById(R.id.vmkoom_edit_SSID);
        vmkoomSSIDInput.setText(vmkoomNameWifiHotspot);

        vmkoomPasswordInput = this.mRootView.findViewById(R.id.vmkoom_edit_password);
        vmkoomPasswordInput.setText(vmkoomPasswordWifiHotspot);

        vmkoomSecuritySwitchToggle = this.mRootView.findViewById(R.id.vmkoom_security_switch_toggle);
        vmkoomSecuritySwitchToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    vmkoomPasswordInput.setEnabled(true);
                    vmkoomPasswordInput.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.black));
                    vmkoomShowPasswordCheckbox.setEnabled(true);
                    vmkoomShowPasswordCheckbox.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                } else {
                    vmkoomPasswordInput.setEnabled(false);
                    vmkoomPasswordInput.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.gray));
                    vmkoomShowPasswordCheckbox.setEnabled(false);
                    vmkoomShowPasswordCheckbox.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
                }
            }
        });

        vmkoomSettingSave = this.mRootView.findViewById(R.id.vmkoom_setting_save);
        vmkoomSettingSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View mRootView) {

//                String tempNameWifiHotspot = sharedPref.getString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, "FreeWiFi");
//                String tempPasswordWifiHotspot = sharedPref.getString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, "11111111");

                vmkoomNameWifiHotspot = vmkoomSSIDInput.getText().toString();
                vmkoomPasswordWifiHotspot = vmkoomPasswordInput.getText().toString();

//                if (tempNameWifiHotspot.equals(vmkoomNameWifiHotspot) && tempPasswordWifiHotspot.equals(vmkoomPasswordWifiHotspot)) {
//                    return;
//                }

                Toast.makeText(getActivity(), "Save!", Toast.LENGTH_LONG).show();

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, vmkoomNameWifiHotspot);
                editor.apply();
                editor.putString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, vmkoomPasswordWifiHotspot);
                editor.apply();

                if (mWifiHotspotEnable) {
                    mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(false, vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            vmkoomStartWifiHotspot();
                        }
                    }, 2000);
                }
            }
        });

        vmkoomPasswordInput.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().trim().length() > 7 && vmkoomSSIDInput.length() > 0) {
                            vmkoomSettingSave.setEnabled(true);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorTextCanSave));
                        } else {
                            vmkoomSettingSave.setEnabled(false);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorTextCannotSave));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

        vmkoomSSIDInput.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().trim().length() > 0 && vmkoomPasswordInput.length() > 7) {
                            vmkoomSettingSave.setEnabled(true);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorTextCanSave));
                        } else {
                            vmkoomSettingSave.setEnabled(false);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorTextCannotSave));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

        // Toggling the show password CheckBox will mask or unmask the password input EditText
        vmkoomShowPasswordCheckbox = this.mRootView.findViewById(R.id.vmkoom_showPassword);
        vmkoomShowPasswordCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!vmkoomShowPasswordCheckbox.isChecked()) {
                    vmkoomPasswordInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    vmkoomPasswordInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    vmkoomPasswordInput.setInputType(InputType.TYPE_CLASS_TEXT);
                    vmkoomPasswordInput.setTransformationMethod(null);
                }
            }
        });
    }

    private static int requestCode_ACTION_MANAGE_WRITE_SETTINGS = 200;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == requestCode_ACTION_MANAGE_WRITE_SETTINGS) {
                if (resultCode == RESULT_OK) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(getApplicationContext())) {
                            vmkoomRetryOpenWifiHotspot();
                        } else {
                            vmkoomStartWifiHotspot();
                        }
                    }
                }
            }
        } catch (Exception ignored) {

        }
    }

    private void vmkoomRetryOpenWifiHotspot() {
        try {
            final CoordinatorLayout coordinatorLayout;
            coordinatorLayout = mRootView.findViewById(R.id.scrollView);
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Unable to Open Free Hotspot", Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View mRootView) {
                            vmkoomStartWifiHotspot();
                        }
                    });

            snackbar.show();
        } catch (Exception ignored) {

        }
    }

    private void vmkoomShareWifiHotspot() {

        try {

            if (vmkoomSSIDInput.length() < 1) {
                Toast.makeText(getActivity(), "No network name",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if (vmkoomSecuritySwitchToggle.isChecked()) {
                if (vmkoomPasswordInput.toString().trim().length() < 8) {
                    Toast.makeText(getActivity(), "Password (at least 8 character)",
                            Toast.LENGTH_LONG).show();
                    return;
                }
            }

            if (mWifiHotspotEnable) {
                vmkoomStopWiFiHotspot();
//                MagicActivity.useMagicActivityToTurnOff(getActivity());
                return;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("Start Wifi Hotspot?")
                        .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                        .setContentText("for the 1st time Tap <font color='black'><b>SETUP</b></font> button to change wifi hotspot configuration")
                        .setCancelText("Cancel")
                        .setConfirmText("OK!")
                        .setNeutralText("Setup")
                        .setNeutralButtonBackgroundColor(Color.BLUE)
                        .showCancelButton(true)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                vmkoomStartWifiHotspot();
                                starthotspotdialog_oreo();
                                sDialog.cancel();
                            }
                        })
                        .setNeutralClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
                                intent.setComponent(cn);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .show();
            } else {
                vmkoomStartWifiHotspot();
                starthotspotdialog();
            }

//            vmkoomStartWifiHotspot();
//            starthotspotdialog();

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    @SuppressLint("SetTextI18n")
    public void vmkoomStartWifiHotspot() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getActivity())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
                    return;
                }
            }
//        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
//        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
//        intent.setComponent(cn);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
            if (mHotUtil != null) {

                String password;
                if (vmkoomSecuritySwitchToggle.isChecked()) {
                    password = vmkoomPasswordWifiHotspot;
                    vmkoomSettingInformation.setText(
                            "Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: " + vmkoomPasswordWifiHotspot);
                } else {
                    password = "";
                    vmkoomSettingInformation.setText(
                            "Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: ");
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                MagicActivity.useMagicActivityToTurnOn(Objects.requireNonNull(getActivity()));
                    vmkoomSettingInformation.setVisibility(View.GONE);
                } else {
                    vmkoomSettingInformation.setVisibility(View.VISIBLE);
                }
//            vmkoomSettingInformation.setVisibility(View.VISIBLE);

                //Start Hotspot
                mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(true, vmkoomNameWifiHotspot, password);
//                MagicActivity.useMagicActivityToTurnOn(getActivity());
                mWifiHotspotEnable = true;

//            Change Image
                vmkoomImageToggle.setImageResource(R.drawable.salsa_wifi_enable);
                bthotspot_off.setVisibility(View.GONE);
                bthotspot_on.setVisibility(View.GONE);


                hotspotconfig.setVisibility(View.GONE);
            }

        } catch (Exception ignored) {

        }
    }

    private void vmkoomStopWiFiHotspot() {
        if (mHotUtil != null) {

            vmkoomSettingInformation.setVisibility(View.GONE);

            //Stop hotspot
            mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(false, vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot);

            //Change Image
            vmkoomImageToggle.setImageResource(R.drawable.salsa_wifi_disable);
            bthotspot_off.setVisibility(View.GONE);
            bthotspot_on.setVisibility(View.GONE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                hotspotconfig.setVisibility(View.GONE);
            } else {
                hotspotconfig.setVisibility(View.VISIBLE);
            }
//            hotspotconfig.setVisibility(View.VISIBLE);
            mWifiHotspotEnable = false;


            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    try {
                        stophotspotdialog2();
                        MagicActivity.useMagicActivityToTurnOff(Objects.requireNonNull(getActivity()));


                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                }
            }, 200);
            WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            Objects.requireNonNull(wifi).setWifiEnabled(true);
        }
    }

    private void starthotspotdialog() {
        new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Start Wifi Hotspot")
                .setContentText("Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: " + vmkoomPasswordWifiHotspot)
                .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    private void starthotspotdialog_oreo() {
        new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Start Wifi Hotspot")
                .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    private void stophotspotdialog() {
        new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Disable Hotspot ?")
                .setContentText("AP Name : FreeHotspot\nPassword : thankyou")
                .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                .setConfirmText("Disable")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
//                        showInterstitial();
                        if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                            AppUtils.getInstance().showAdmobAdsFullBanner(null);
                        }
                    }
                })
                .setCancelText("No")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();

                    }
                })
                .show();
    }


    private void stophotspotdialog2() {
        final SweetAlertDialog pDialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading");
        pDialog.show();
        pDialog.setCancelable(false);
        new CountDownTimer(800 * 2, 800) {
            public void onTick(long millisUntilFinished) {
                // you can change the progress bar color by ProgressHelper every 800 millis
                i++;
                switch (i) {
                    case 0:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.blue_btn_bg_color));
                        break;
                    case 1:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_50));
                        break;
                    case 2:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                    case 3:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_20));
                        break;
                    case 4:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_blue_grey_80));
                        break;
                    case 5:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.warning_stroke_color));
                        break;
                    case 6:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                }
            }

            public void onFinish() {
                i = -1;

                pDialog.setTitleText("Disable Hotspot")
                        .setContentText("Personal Mobile Hotspot already Turn Off")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                showInterstitial();
                            }
                        })
                        .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                        .changeAlertType(SweetAlertDialog.CUSTOM_IMAGE_TYPE);

            }
        }.start();
    }

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }

    /**
     * Populates a {@link UnifiedNativeAdView} object with data from a given
     * {@link UnifiedNativeAd}.
     *
     * @param nativeAd the object containing the ad's assets
     * @param adView   the view to be populated
     */
    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        try {
            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
            // have a video asset.
            VideoController vc = nativeAd.getVideoController();

            // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
            // VideoController will call methods on this object when events occur in the video
            // lifecycle.
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                public void onVideoEnd() {
                    // Publishers should allow native ads to complete video playback before refreshing
                    // or replacing them with another ad in the same UI location.
//                refresh.setEnabled(true);
//                videoStatus.setText("Video status: Video playback has ended.");
                    super.onVideoEnd();
                }
            });

            MediaView mediaView = adView.findViewById(R.id.ad_media);
            ImageView mainImageView = adView.findViewById(R.id.ad_image);

            // Apps can check the VideoController's hasVideoContent property to determine if the
            // NativeAppInstallAd has a video asset.
            if (vc.hasVideoContent()) {
                adView.setMediaView(mediaView);
                mainImageView.setVisibility(View.GONE);
//            videoStatus.setText(String.format(Locale.getDefault(),
//                    "Video status: Ad contains a %.2f:1 video asset.",
//                    vc.getAspectRatio()));
            } else {
                adView.setImageView(mainImageView);
                mediaView.setVisibility(View.GONE);

                // At least one image is guaranteed.
                List<com.google.android.gms.ads.formats.NativeAd.Image> images = nativeAd.getImages();
                mainImageView.setImageDrawable(images.get(0).getDrawable());

//            refresh.setEnabled(true);
//            videoStatus.setText("Video status: Ad does not contain a video asset.");
            }

            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
            adView.setBodyView(adView.findViewById(R.id.ad_body));
            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(adView.findViewById(R.id.ad_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.ad_price));
            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
//        adView.setStoreView(adView.findViewById(R.id.ad_store));
            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

            // Some assets are guaranteed to be in every UnifiedNativeAd.
            ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
            // check before trying to display them.
            if (nativeAd.getIcon() == null) {
                adView.getIconView().setVisibility(View.GONE);
            } else {
                ((ImageView) adView.getIconView()).setImageDrawable(
                        nativeAd.getIcon().getDrawable());
                adView.getIconView().setVisibility(View.VISIBLE);
            }

//        if (nativeAd.getPrice() == null) {
//            adView.getPriceView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getPriceView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
//        }

//        if (nativeAd.getStore() == null) {
//            adView.getStoreView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getStoreView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
//        }

            if (nativeAd.getStarRating() == null) {
                adView.getStarRatingView().setVisibility(View.INVISIBLE);
            } else {
                ((RatingBar) adView.getStarRatingView())
                        .setRating(nativeAd.getStarRating().floatValue());
                adView.getStarRatingView().setVisibility(View.VISIBLE);
            }

            if (nativeAd.getAdvertiser() == null) {
                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
            } else {
                ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
                adView.getAdvertiserView().setVisibility(View.VISIBLE);
            }

            adView.setNativeAd(nativeAd);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    /**
     * Creates a request for a new native ad based on the boolean parameters and calls the
     * corresponding "populate" method when one is successfully returned.
     */
    private void refreshAd() {
        try {
//        refresh.setEnabled(false);


            AdLoader.Builder builder2 = new AdLoader.Builder(Objects.requireNonNull(getActivity()), getResources().getString(R.string.admob_large_native_id));

            builder2.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                // OnUnifiedNativeAdLoadedListener implementation.
//                @Override
//                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd2) {
//                    try {
//                        FrameLayout frameLayout2 =
//                                mView.findViewById(R.id.fl_adplaceholder2);
//                        UnifiedNativeAdView adView2 = (UnifiedNativeAdView) getLayoutInflater()
//                                .inflate(R.layout.ad_unified, null);
//                        populateUnifiedNativeAdView(unifiedNativeAd2, adView2);
//                        frameLayout2.removeAllViews();
//                        frameLayout2.addView(adView2);
//                    } catch (Exception ignored) {
//                        Crashlytics.logException(ignored);
//                    }
//                }
                @Override
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    NativeTemplateStyle styles = new
                            NativeTemplateStyle.Builder().build();

                    TemplateView template = mRootView.findViewById(R.id.my_template);
                    template.setStyles(styles);
                    template.setNativeAd(unifiedNativeAd);
                    template.setVisibility(View.VISIBLE);
                }

            });


            VideoOptions videoOptions = new VideoOptions.Builder()
                    .build();

            NativeAdOptions adOptions = new NativeAdOptions.Builder()
                    .setVideoOptions(videoOptions)
                    .build();

            builder2.withNativeAdOptions(adOptions);

            AdLoader adLoader2 = builder2.withAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
//                refresh.setEnabled(true);
//                Toast.makeText(MainActivity.this, "Failed to load native ad: "
//                        + errorCode, Toast.LENGTH_SHORT).show();
                }
            }).build();

            adLoader2.loadAd(new AdRequest.Builder().build());

//        videoStatus.setText("");

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }


    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(Objects.requireNonNull(getActivity()));
        adtView.setAdUnitId(getResources().getString(R.string.admob_adaptive_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);

        return AdSize.getCurrentOrientationBannerAdSizeWithWidth(getActivity(), adWidth);
    }
}
