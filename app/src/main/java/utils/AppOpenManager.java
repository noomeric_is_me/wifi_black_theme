package utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.appopen.AppOpenAd;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;


import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.main.CustomSlide;

import static androidx.lifecycle.Lifecycle.Event.ON_START;
import static com.thefinestartist.utils.content.ContextUtil.getResources;

/**
 * Prefetches App Open Ads.
 */
public class AppOpenManager implements Application.ActivityLifecycleCallbacks, LifecycleObserver {
    private static final String LOG_TAG = "utils.AppOpenManager";
    private static final String AD_UNIT_ID = "ca-app-pub-3940256099942544/3419835294";
    private AppOpenAd appOpenAd = null;
    private long loadTime = 0;
    private AppOpenAd.AppOpenAdLoadCallback loadCallback;
    private Activity currentActivity;
    private static boolean isShowingAd = false;
    private static int appOpenAd_count = 0;
    private static int appOpenAd_count_maximum = 3;
    private final Application myApplication;

    /** Constructor */
    public AppOpenManager(AppController myApplication) {
        this.myApplication = myApplication;
        this.myApplication.registerActivityLifecycleCallbacks(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    /**
     * Request an ad
     */
    private void fetchAd() {
        // Have unused ad, no need to fetch another.
        if (isAdAvailable()) {
            return;
        }

        loadCallback =
                new AppOpenAd.AppOpenAdLoadCallback() {
                    /**
                     * Called when an app open ad has loaded.
                     *
                     * @param ad the loaded app open ad.
                     */
                    @Override
                    public void onAppOpenAdLoaded(AppOpenAd ad) {
                        AppOpenManager.this.appOpenAd = ad;
                        AppOpenManager.this.loadTime = (new Date()).getTime();
                    }

                    /**
                     * Called when an app open ad has failed to load.
                     *
                     * @param loadAdError the error.
                     */
                    @Override
                    public void onAppOpenAdFailedToLoad(LoadAdError loadAdError) {
                        // Handle the error.
                        Log.d(LOG_TAG,"error in loading");
                    }

                };
        AdRequest request = getAdRequest();
        AppOpenAd.load(
//                myApplication, AD_UNIT_ID, request,
                myApplication, (getResources().getString(R.string.admob_appopen_id)), request,
                AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT, loadCallback);
    }

    /**
     * Shows the ad if one isn't already showing.
     */
    private void showAdIfAvailable() {
        // Only show ad if there is not already an app open ad currently showing
        // and an ad is available.
        if (!isShowingAd && isAdAvailable()&& CustomSlide.getMSettingPermission()) {
            Log.d(LOG_TAG, "Will show ad.");

            FullScreenContentCallback fullScreenContentCallback =
                    new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            // Set the reference to null so isAdAvailable() returns false.
                            AppOpenManager.this.appOpenAd = null;
                            isShowingAd = false;
                            fetchAd();
                            Log.d(LOG_TAG, "onAdDismissed ad.");
                        }

                        @Override
                        public void onAdFailedToShowFullScreenContent(AdError adError) {
                            Log.d(LOG_TAG, "onAdFailed ad.");
                        }

                        @Override
                        public void onAdShowedFullScreenContent() {
                            isShowingAd = true;
                            Log.d(LOG_TAG, "onAdShowed ad.");
                        }
                    };

//            if(appOpenAd_count >= appOpenAd_count_maximum){
//                appOpenAd.show(currentActivity, fullScreenContentCallback);
////                Toast.makeText(currentActivity, "show ad :)", Toast.LENGTH_SHORT).show();
//                appOpenAd_count = 0; //reset
//                Log.d(LOG_TAG, "show ad + appOpenAd_count reset");
//
//
//            }else {
//                appOpenAd_count++;
//                Log.d(LOG_TAG, "appOpenAd_count"+appOpenAd_count);
////                Toast.makeText(currentActivity, "appOpenAd_count"+appOpenAd_count, Toast.LENGTH_SHORT).show();
//            }
            appOpenAd.show(currentActivity, fullScreenContentCallback);
//            Toast.makeText(currentActivity, "show ad :)", Toast.LENGTH_SHORT).show();

        } else {
            Log.d(LOG_TAG, "Can not show ad.");
            fetchAd();
        }
    }

    /**
     * Creates and returns ad request.
     */
    private AdRequest getAdRequest() {
        return new AdRequest.Builder().build();
    }

    /** Utility method to check if ad was loaded more than n hours ago. */
    private boolean wasLoadTimeLessThanNHoursAgo(long numHours) {
        long dateDifference = (new Date()).getTime() - this.loadTime;
        long numMilliSecondsPerHour = 3600000;
        return (dateDifference < (numMilliSecondsPerHour * numHours));
    }
    /**
     * Utility method that checks if ad exists and can be shown.
     */
    private boolean isAdAvailable() {
//        return appOpenAd != null;
        return appOpenAd != null && wasLoadTimeLessThanNHoursAgo(4);
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        currentActivity = activity;

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        currentActivity = activity;

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        currentActivity = null;

    }
    /** LifecycleObserver methods */
    @OnLifecycleEvent(ON_START)
    void onStart() {

            showAdIfAvailable();
//        Toast.makeText(currentActivity, "On Start :)", Toast.LENGTH_SHORT).show();
        Log.d(LOG_TAG, "onStart");
    }
}