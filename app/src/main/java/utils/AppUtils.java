package utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdScrollView;
import com.facebook.ads.NativeAdView;
import com.facebook.ads.NativeAdsManager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.services.model.AppConfig;
import wimosalsafifreewifi.services.model.AppNetworkInfo;


public class AppUtils {
    public static final int NONE_TYPE = -1;
    public static final int FB_ADS_TYPE = 0;
    public static final int ADMOB_ADS_TYPE = 1;
    public static int adsNetworkType = FB_ADS_TYPE;
    public static int interstitial_count_disable = -1;
    public static int interstitial_count_maximum = 3;
    public static int interstitial_count = interstitial_count_maximum;
    public static boolean ads_interstitial_show_all;
    public static final String ADS_TYPE_KEY = "ads_type_key";
    public static final String RATING_APP_TYPE_KEY = "rating_app_type_key";
    public static final String RATING_APP_COUNT_TYPE_KEY = "rating_app_count_type_key";
    public static final String SHOW_INTRO_SPEEDTEST_HISTORY_TYPE_KEY = "show_intro_speedtest_history_type_key";
    public static final String SHOW_INTRO_SPEEDTEST_SHARE_TYPE_KEY = "show_intro_speedtest_share_type_key";
    public static final String SHOW_INTRO_SPEEDTEST_SPEEDBOOSTER_TYPE_KEY = "show_intro_speedtest_speedbooster_type_key";
    public static final String SHOW_INTRO_WIFI_HOTSPOT_TYPE_KEY = "show_intro_wifi_hotspot_type_key";
    public static final String SHOW_INTRO_CLOSE_WIFI_HOTSPOT_TYPE_KEY = "show_intro_close_wifi_hotspot_type_key";

    public static AppConfig appConfig;
    public static AppNetworkInfo appNetworkInfo;
    public static String defaultads;
    public static String source;
    public static String ads_banner_id;
    public static String ads_interstitial_id;
    public static String ads_native_id;

    public static String speedtest_server = "speedtest-cmi1.ais-idc.net";

    private static AppUtils mInstance;
    private BaseAdListener mBaseAdListener;

    public interface BaseAdListener{
        void onAdClosed();
    }

    public static AppUtils getInstance(){
        if(mInstance == null){
            mInstance = new AppUtils();
//            AdSettings.addTestDevice("30cbfa8cad9c24dfcad5d345aaf46fa6");
//            AdSettings.addTestDevice("70ac77545bb0041f24f04750cbcfbb85");
        }
        return mInstance;
    }

    public void setAppNetworkInfo(AppNetworkInfo appNetworkInfo) {
        AppUtils.appNetworkInfo = appNetworkInfo;
    }

    public AppNetworkInfo getAppNetworkInfo() {
        if(appNetworkInfo == null){
            appNetworkInfo = new AppNetworkInfo();
        }
        return appNetworkInfo;
    }

    private final Random RANDOM = new Random();

    public int getRandomColor(int currentColor) {
//        int color;
//        switch (RANDOM.nextInt(5)) {
//            default:
//            case 0:
//                color = Color.parseColor("#0AAE9F");
//                if(currentColor == color){
//                    return Color.parseColor("#FFC74B46");
//                }
//                return color;
////                return Color.parseColor("#0AAE9F");
//            case 1:
//                color = Color.parseColor("#FFC74B46");
//                if(currentColor == color){
//                    return Color.parseColor("#FFF4842D");
//                }
//                return color;
////                return Color.parseColor("#FFC74B46");
//            case 2:
//                color = Color.parseColor("#FFF4842D");
//                if(currentColor == color){
//                    return Color.parseColor("#FF3F9FE0");
//                }
//                return color;
////                return Color.parseColor("#FFF4842D");
//            case 3:
//                color = Color.parseColor("#FF3F9FE0");
//                if(currentColor == color){
//                    return Color.parseColor("#FF5161BC");
//                }
//                return color;
////                return Color.parseColor("#FF3F9FE0");
//            case 4:
//                color = Color.parseColor("#FF5161BC");
//                if(currentColor == color){
//                    return Color.parseColor("#0AAE9F");
//                }
//                return color;
////                return Color.parseColor("#FF5161BC");
//        }


//    <color name="tab_1_color">#ffa800</color>
//    <color name="tab_2_color">#f35fa8</color>
//    <color name="tab_3_color">#60aa49</color>
//    <color name="tab_4_color">#ff6c00</color>
//    <color name="tab_5_color">#5b95f5</color>
//    <color name="tab_6_color">#8e35e1</color>
//    <color name="tab_7_color">#f13838</color>

        int color;
        switch (RANDOM.nextInt(7)) {
            default:
            case 0:
                color = Color.parseColor("#ffa800");
                if(currentColor == color){
                    return Color.parseColor("#f35fa8");
                }
                return color;

            case 1:
                color = Color.parseColor("#f35fa8");
                if(currentColor == color){
                    return Color.parseColor("#60aa49");
                }
                return color;

            case 2:
                color = Color.parseColor("#60aa49");
                if(currentColor == color){
                    return Color.parseColor("#ff6c00");
                }
                return color;

            case 3:
                color = Color.parseColor("#ff6c00");
                if(currentColor == color){
                    return Color.parseColor("#5b95f5");
                }
                return color;

            case 4:
                color = Color.parseColor("#5b95f5");
                if(currentColor == color){
                    return Color.parseColor("#8e35e1");
                }
                return color;

            case 5:
                color = Color.parseColor("#8e35e1");
                if(currentColor == color){
                    return Color.parseColor("#f13838");
                }
                return color;

            case 6:
                color = Color.parseColor("#f13838");
                if(currentColor == color){
                    return Color.parseColor("#ffa800");
                }
                return color;
        }
    }


    public void openPlaystore(Context context) {

        try {
            Uri marketUri = Uri.parse("market://details?id=" + context.getPackageName());
            context.startActivity(new Intent("android.intent.action.VIEW", marketUri));
        } catch (ActivityNotFoundException var4) {
            Toast.makeText(context, "Couldn\'t find PlayStore on this device", Toast.LENGTH_SHORT).show();
        }

    }

    //Facebook Ads
    private com.facebook.ads.InterstitialAd mFBinterstitialAd;
    public com.facebook.ads.AdView showFBAdsBanner(Context context,final RelativeLayout mAdView){
        com.facebook.ads.AdView adViewFB = new com.facebook.ads.AdView(
                context,
                context.getResources().getString(R.string.fb_id_ads_banner)
                , AdSize.BANNER_HEIGHT_50);
        mAdView.addView(adViewFB);
        adViewFB.loadAd();
        return adViewFB;
    }

    public com.facebook.ads.InterstitialAd FBfullBannerInit(final Context context) {

        com.facebook.ads.InterstitialAd FBinterstitialAd = new com.facebook.ads.InterstitialAd(
                context, context.getResources().getString(R.string.fb_id_ads_interstitial));

        FBinterstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                if(mBaseAdListener != null){
                    mBaseAdListener.onAdClosed();
                }
                //Re init facebook Ads
                FBfullBannerInit(context);
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad failed to load
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad is loaded and ready to be displayed
                // You can now display the full screen add using this code:

            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
        FBinterstitialAd.loadAd();
        mFBinterstitialAd = FBinterstitialAd;
        return FBinterstitialAd;
    }

    public void showAdsFullBanner(BaseAdListener baseAdListener){

        Log.d("NTL","interstitial_count = " + interstitial_count);

        mBaseAdListener = baseAdListener;

        if(adsNetworkType == FB_ADS_TYPE){

            if(checkForFacebook(baseAdListener)) return;
            if(checkForAdmob(baseAdListener)) return;

        } else if(adsNetworkType == ADMOB_ADS_TYPE){

            if(checkForAdmob(baseAdListener)) return;
            if(checkForFacebook(baseAdListener)) return;

        }
//        if(mFBinterstitialAd != null && mFBinterstitialAd.isAdLoaded()) {
//            showFBAdsFullBanner(baseAdListener);
//            return;
//        } else {
//            FBfullBannerInit(AppController.getInstance().getAppContext());
//        }
//
//        if(mAdmobInterstitialAd != null && mAdmobInterstitialAd.isLoaded()){
//            showAdmobAdsFullBanner(baseAdListener);
//            return;
//        } else {
//            requestNewInterstitial();
//        }

        Log.d("NTL", "showAdsFullBanner No Fill", null);
        //All ad no fill unavailable
        if(mBaseAdListener != null){
            mBaseAdListener.onAdClosed();
        }
    }

    private boolean checkForFacebook(BaseAdListener baseAdListener){
        if(mFBinterstitialAd != null && mFBinterstitialAd.isAdLoaded()) {
            showFBAdsFullBanner(baseAdListener);
            return true;
        } else {
            FBfullBannerInit(AppController.getInstance().getAppContext());
            return false;
        }
    }

    private boolean checkForAdmob(BaseAdListener baseAdListener){
        if(mPublisherInterstitialAd != null && mPublisherInterstitialAd.isLoaded()){
            showAdmobAdsFullBanner(baseAdListener);
            return true;
        } else {
            fullAdmobAdsBannerInit(AppController.getInstance().getAppContext());
            return false;
        }
    }

    public void showFBAdsFullBanner(BaseAdListener baseAdListener){
        try {
            mBaseAdListener = baseAdListener;

            if(!FacebookSdk.isInitialized()) {
                if(mBaseAdListener != null){
                    mBaseAdListener.onAdClosed();
                }
                return;
            }

            if(interstitial_count_maximum == interstitial_count_disable) {
                if(mBaseAdListener != null){
                    mBaseAdListener.onAdClosed();
                }
                return;
            }


            if(interstitial_count >= interstitial_count_maximum){

                Log.d("NTL","Show Ad Now");

                if(mFBinterstitialAd != null && mFBinterstitialAd.isAdLoaded()) {
                    mFBinterstitialAd.show();
                    interstitial_count = 0; //reset
                }else{

                    //Re init facebook Ads
                    FBfullBannerInit(AppController.getInstance().getAppContext());

                    if(mBaseAdListener != null){
                        mBaseAdListener.onAdClosed();
                    }
                }

            } else {
                interstitial_count++;

                if(mBaseAdListener != null){
                    mBaseAdListener.onAdClosed();
                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

//    private LinearLayout nativeAdContainer;
//    private LinearLayout adView;
//    private NativeAd nativeAd;
    public void showFBAdsNativeInit(final Context context, LinearLayout nativeAdLayout){
        final LinearLayout nativeAdContainer = nativeAdLayout;
        final NativeAd nativeAd = new NativeAd(context, context.getResources().getString(R.string.fb_id_ads_native));
        nativeAd.setAdListener(new com.facebook.ads.AdListener() {

            @Override
            public void onError(Ad ad, AdError error) {
                // Ad error callback
            }

            @Override
            public void onAdLoaded(Ad ad) {

                try {

                    if (nativeAd != null) {
                        nativeAd.unregisterView();
                    }

                    // Add the Ad view into the ad container.
//                nativeAdContainer = (LinearLayout) rootView.findViewById(R.id.native_ad_container);
                    LayoutInflater inflater = LayoutInflater.from(context);
                    // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                    LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_ad_layout_v1, nativeAdContainer, false);
                    nativeAdContainer.addView(adView);


                    // Create native UI using the ad metadata.
                    ImageView nativeAdIcon = (ImageView) adView.findViewById(R.id.native_ad_icon);
                    TextView nativeAdTitle = (TextView) adView.findViewById(R.id.native_ad_title);
                    MediaView nativeAdMedia = (MediaView) adView.findViewById(R.id.native_ad_media);
                    TextView nativeAdSocialContext = (TextView) adView.findViewById(R.id.native_ad_social_context);
                    TextView nativeAdBody = (TextView) adView.findViewById(R.id.native_ad_body);
                    Button nativeAdCallToAction = (Button) adView.findViewById(R.id.native_ad_call_to_action);

                    // Set the Text.
                    nativeAdTitle.setText(nativeAd.getAdTitle());
                    nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
                    nativeAdBody.setText(nativeAd.getAdBody());
                    nativeAdCallToAction.setText(nativeAd.getAdCallToAction());

                    // Download and display the ad icon.
                    NativeAd.Image adIcon = nativeAd.getAdIcon();
                    NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);

                    // Download and display the cover image.
                    nativeAdMedia.setNativeAd(nativeAd);

                    // Add the AdChoices icon
                    LinearLayout adChoicesContainer = (LinearLayout) adView.findViewById(R.id.ad_choices_container);
                    AdChoicesView adChoicesView = new AdChoicesView(context, nativeAd, true);
                    adChoicesContainer.addView(adChoicesView);

                    // Register the Title and CTA button to listen for clicks.
                    List<View> clickableViews = new ArrayList<>();
                    clickableViews.add(nativeAdTitle);
                    clickableViews.add(nativeAdCallToAction);
                    if(nativeAd != null && nativeAdContainer != null){
                        nativeAd.registerViewForInteraction(nativeAdContainer, clickableViews);
                    }

                } catch (Exception ignored) {
                    Crashlytics.logException(ignored);
                }

            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

        });

        // Request an ad
        nativeAd.loadAd();
    }

    public void showFBAdsNativeSmallInit(final Context context, final LinearLayout nativeAdContainer){
        final NativeAd nativeAd = new NativeAd(context, context.getResources().getString(R.string.fb_id_ads_native));
        nativeAd.setAdListener(new com.facebook.ads.AdListener() {

            @Override
            public void onError(Ad ad, AdError error) {
                // Ad error callback
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Add the Ad view into the ad container.
//                nativeAdContainer = (LinearLayout) rootView.findViewById(R.id.native_ad_container);
                LayoutInflater inflater = LayoutInflater.from(context);
                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_ad_small_layout, nativeAdContainer, false);
                nativeAdContainer.addView(adView);

                // Create native UI using the ad metadata.
                ImageView nativeAdIcon = (ImageView) adView.findViewById(R.id.native_ad_icon);
                TextView nativeAdTitle = (TextView) adView.findViewById(R.id.native_ad_title);
//                MediaView nativeAdMedia = (MediaView) adView.findViewById(R.id.native_ad_media);
//                TextView nativeAdSocialContext = (TextView) adView.findViewById(R.id.native_ad_social_context);
                TextView nativeAdBody = (TextView) adView.findViewById(R.id.native_ad_body);
                Button nativeAdCallToAction = (Button) adView.findViewById(R.id.native_ad_call_to_action);

                // Set the Text.
                nativeAdTitle.setText(nativeAd.getAdTitle());
//                nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
                nativeAdBody.setText(nativeAd.getAdBody());
                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());

                // Download and display the ad icon.
                NativeAd.Image adIcon = nativeAd.getAdIcon();
                NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);

                // Download and display the cover image.
//                nativeAdMedia.setNativeAd(nativeAd);

                // Add the AdChoices icon
                LinearLayout adChoicesContainer = (LinearLayout) adView.findViewById(R.id.ad_choices_container);
                AdChoicesView adChoicesView = new AdChoicesView(context, nativeAd, true);
                adChoicesContainer.addView(adChoicesView);

                // Register the Title and CTA button to listen for clicks.
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(nativeAdTitle);
                clickableViews.add(nativeAdCallToAction);
                nativeAd.registerViewForInteraction(nativeAdContainer, clickableViews);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

        });

        // Request an ad
        nativeAd.loadAd();
    }

    private NativeAdsManager manager;
    private NativeAdScrollView nativeAdScrollView;
    //NativeAdView.Type.HEIGHT_300
    private void showFBAdsNativeScroolHorizontalInit(final Context context,
                                                     final NativeAdView.Type NativeAdViewType){
        manager = new NativeAdsManager(context, context.getResources().getString(R.string.fb_id_ads_native), 5);
        manager.setListener(new NativeAdsManager.Listener() {
            @Override
            public void onAdsLoaded() {
                if(manager.isLoaded()){
                    nativeAdScrollView = new NativeAdScrollView(context, manager, NativeAdViewType);
                }
            }

            @Override
            public void onAdError(AdError adError) {
                // Ad error callback
            }
        });
        manager.loadAds(NativeAd.MediaCacheFlag.ALL);
    }

    public NativeAdScrollView getFBAdsNativeScroolHorizontal(final Context context,
                                                             final NativeAdView.Type NativeAdViewType){

        if(nativeAdScrollView == null){
            showFBAdsNativeScroolHorizontalInit(context,NativeAdViewType);
        }
        return nativeAdScrollView;
    }

    //Admob Ads
    private InterstitialAd mAdmobInterstitialAd;
    private PublisherInterstitialAd mPublisherInterstitialAd;

    public void fullAdmobAdsBannerInit(Context context){
        if(mPublisherInterstitialAd != null){
            return;
        }
        mPublisherInterstitialAd = new PublisherInterstitialAd(context);
        mPublisherInterstitialAd.setAdUnitId(context.getResources().getString(R.string.admob_interstitial_id));

        mPublisherInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                if(mBaseAdListener != null){
                    mBaseAdListener.onAdClosed();
                }
            }
        });

        requestNewInterstitial();
    }

    public void showAdmobAdsFullBanner(BaseAdListener baseAdListener){

        if(interstitial_count_maximum == interstitial_count_disable) {
            if(mBaseAdListener != null){
                mBaseAdListener.onAdClosed();
            }
            return;
        }

        if(interstitial_count >= interstitial_count_maximum){

            Log.d("NTL","Show Ad Now");

            mBaseAdListener = baseAdListener;
            if(mPublisherInterstitialAd != null && mPublisherInterstitialAd.isLoaded()){
                mPublisherInterstitialAd.show();
                interstitial_count = 0; //reset
            } else {

                requestNewInterstitial();

                if(mBaseAdListener != null){
                    mBaseAdListener.onAdClosed();
                }
            }

        } else {
            interstitial_count++;

            if(mBaseAdListener != null){
                mBaseAdListener.onAdClosed();
            }
        }
    }

    private void requestNewInterstitial() {
        PublisherAdRequest adRequest = new PublisherAdRequest.Builder()
//                .addTestDevice("6B2F57DA4E29610F7B1E2733F3D4F804")
                .build();

        if(mPublisherInterstitialAd != null){
            mPublisherInterstitialAd.loadAd(adRequest);
        }
    }

    public void showAdsBanner(AdView mAdView){
//        Location location = AppController.getInstance().getmLastLocation();

        AdRequest.Builder builder = new AdRequest.Builder();

//        if(location != null){
//            builder.setLocation(location);
//        }
//        builder.setGender(AdRequest.GENDER_MALE);
        AdRequest request = builder.build();
        mAdView.loadAd(request);


//        AdRequest adRequest = new AdRequest.Builder()
////                .addTestDevice("6B2F57DA4E29610F7B1E2733F3D4F804")
//                .build();
//        mAdView.loadAd(adRequest);
    }

    public void showNativeAdsBanner(NativeExpressAdView mAdView){
//        Location location = AppController.getInstance().getmLastLocation();

        AdRequest.Builder builder = new AdRequest.Builder();

//        if(location != null){
//            builder.setLocation(location);
//        }
//        builder.setGender(AdRequest.GENDER_MALE);
        AdRequest request = builder.build();
        mAdView.loadAd(request);


//        AdRequest adRequest = new AdRequest.Builder()
////                .addTestDevice("6B2F57DA4E29610F7B1E2733F3D4F804")
//                .build();
//
//        mAdView.loadAd(adRequest) ;
    }
}
