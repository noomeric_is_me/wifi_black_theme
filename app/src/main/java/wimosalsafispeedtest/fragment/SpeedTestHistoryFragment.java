package wimosalsafispeedtest.fragment;

import android.content.Context;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import java.util.ArrayList;
import java.util.List;

import wimosalsafispeedtest.internet.SpeedTestSQLite;
import wimosalsafispeedtest.internet.SpeedTestSQLiteData;

/**
 * A placeholder fragment containing a simple view.
 */
public class SpeedTestHistoryFragment extends Fragment {

    private SpeedTestSQLite db;
    private String date, time, download, upload, ping;
    private ListView lv;
    private int counter = 0;
    private ArrayList<String> datelist = new ArrayList<String>();
    private ArrayList<String> timelist = new ArrayList<String>();
    private ArrayList<String> downloadlist = new ArrayList<String>();
    private ArrayList<String> uploadlist = new ArrayList<String>();
    private ArrayList<String> pinglist = new ArrayList<String>();

    public SpeedTestHistoryFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        lv = (ListView) view.findViewById(R.id.placelist);

        db = new SpeedTestSQLite(getActivity());
        List<SpeedTestSQLiteData> bean = db.getAllitems();
        counter = bean.size();

        //Show or hide if no history found
        TextView text_no_history = (TextView) view.findViewById(R.id.text_no_history);
        if(counter == 0) {
            text_no_history.setVisibility(View.VISIBLE);
        }else {
            text_no_history.setVisibility(View.GONE);
        }

        for (SpeedTestSQLiteData bn : bean) {

            date = bn.getDate();
            time = bn.getTime();
            download = bn.getDownload();
            upload = bn.getUpload();
            ping = bn.getPing();

            datelist.add(date);
            timelist.add(time);
            downloadlist.add(download);
            uploadlist.add(upload);
            pinglist.add(ping);
        }

        SpeedTestHistoryFragment.ImageAdapter img = new SpeedTestHistoryFragment.ImageAdapter(getActivity());
        lv.setAdapter(img);

        return view;
    }

    public class ImageAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public ImageAdapter(Context c) {
            mInflater = LayoutInflater.from(c);

        }

        @Override
        public int getCount() {
            return datelist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            SpeedTestHistoryFragment.ImageAdapter.ViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_history, null);

                holder = new SpeedTestHistoryFragment.ImageAdapter.ViewHolder();
                holder.date = (TextView) convertView.findViewById(R.id.textDate);
                holder.time = (TextView) convertView.findViewById(R.id.textTime);
                holder.download = (TextView) convertView.findViewById(R.id.textDl);
                holder.upload = (TextView) convertView.findViewById(R.id.textUl);
                holder.ping = (TextView) convertView.findViewById(R.id.textPing);

                convertView.setTag(holder);

            } else {
                holder = (SpeedTestHistoryFragment.ImageAdapter.ViewHolder) convertView.getTag();
            }

            // System.out.println("title0--" + titlelist.get(position));

            holder.date.setText(datelist.get(position));
            holder.time.setText(timelist.get(position));
            holder.download.setText(downloadlist.get(position));
            holder.upload.setText(uploadlist.get(position));
            holder.ping.setText(pinglist.get(position));

            return convertView;
        }

        class ViewHolder {
            TextView date, time, download, upload, ping;

        }

    }
}
