package wimosalsafispeedtest.fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.ShareEvent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import wimosalsafifreewifi.application.AppController;
import wimosalsafispeedtest.internet.CircularProgressBar;
import wimosalsafispeedtest.internet.CreateDatabaseAsynctask;
import wimosalsafispeedtest.internet.SpeedTestSQLite;
import wimosalsafispeedtest.internet.SpeedTestSQLiteData;
import wimosalsafispeedtest.speedtestminilib.ProgressReportListener;
import wimosalsafispeedtest.speedtestminilib.SpeedTestMini;
import wimosalsafispeedtest.util.FeedXmlUtil;
import utils.AppUtils;

import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;

public class SpeedTestMiniFragment extends Fragment {

    private Button buttonTest, buttonFinished,buttonShare;
    private LinearLayout mLogoapp;
    private TextView download;
    private TextView upload;
    private TextView ping;

    private ScrollView scrollView;
    private ConnectivityManager connManager;
    private String tmp, dateDb = "", timeDb = "", downloadDb, uploadDb, pingDb;
    private LocationManager mLocationManager;
    private float LOCATION_REFRESH_DISTANCE = 0;
    private long LOCATION_REFRESH_TIME = 0;
    private ProgressDialog dialog;
    private String best;
    private ArrayList<ArrayList<Float>> nearestlist;
    private ArrayList<Object> serverXML;
    private int DISTANCE = 0;
    private int INDEX = 1;
    private int counter = 0;
    private String date = "";
    private AlphaAnimation anim;
    private ImageView reset;
    private CircularProgressBar circular;
    private TextView textviewStatus,textviewUnit;
    private LinearLayout display;
    private RelativeLayout  speedTestFragment;
    private ArrayList<String> datelist = new ArrayList<String>();
    private SpeedTestSQLite db;
    private View mRootView;
    private ShareDialog mShareDialog;
    private SharedPreferences sharedPref;
    private PublisherAdView mPublisherAdView;
    private FrameLayout adContainerView;
    private AdView adtView;
//    private FloatingActionButton fab_speed_booster;

    public ScrollView getScrollView() {
        //scrollView = (ScrollView) mRootView.findViewById(R.id.scrollView1);
        return scrollView;
    }


    private class SpeedTestDownloadTask extends AsyncTask<String, Long, Long> {
        @Override
        protected void onPreExecute() {
            download.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.orange));
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(String... params) {

            publishProgress(0L, 0L);
            SpeedTestMini mini = new SpeedTestMini(params[0], Integer.parseInt(params[1]));
            mini.setProgressReportListener(new ProgressReportListener() {
                Long[] data = new Long[3];
                long dlbits_last = 0;
                long ulbits_last = 0;
                long dl_Percentage = 0;

                @Override
                public void reportCurrentDownloadSpeed(long bits) {
                    data[0] = bits;
                    data[1] = dl_Percentage;
                    SpeedTestDownloadTask.this.publishProgress(data);
                    dlbits_last = bits;
                }

                @Override
                public void reportCurrentUploadSpeed(long bits) {
                }

                @Override
                public void reportCurrentDownloadProgress(long percentage) {
                    data[0] = dlbits_last;
                    data[1] = percentage;
                    SpeedTestDownloadTask.this.publishProgress(data);
                    dl_Percentage = percentage;
                }

                @Override
                public void reportCurrentUploadPercentage(long l) {

                }

            });

            try {
                mini.doDownloadtest();
                return 0L;
            } catch (IOException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }

            return 0L;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            String suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.bits_sec);
            double sout = values[0];
            long percentage = values[1];
            if (sout > 1000000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.mbps);
                sout /= 1000000.0;
            } else if (sout > 1000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.kbps);
                sout /= 1000.0;
            }

            download.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.download)+"\n" + String.format("%.2f", sout) + suffix + "\n");
            circular.setProgress((int) percentage);

//            Log.d("downloadDb",circular.getProgress() + "");

//            circular.setTitle(String.format("%.2f", sout) + suffix);
//            circular.setSubTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.download));
            downloadDb=(String.format("%.2f", sout)+suffix);

            textviewStatus.setText(String.format("%.2f", sout));
            textviewUnit.setText(suffix);
        }

        @Override
        protected void onPostExecute(Long result) {
            new SpeedTestUploadTask().execute(host, "80");
            download.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.NativeGreen));

        }
    }

    private class SpeedTestUploadTask extends AsyncTask<String, Long, Long> {
        double ul_avg = 0;
        int cycle = 0;

        @Override
        protected void onPreExecute() {
            upload.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.orange));
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(String... params) {

            publishProgress(0L, 0L);
            SpeedTestMini mini = new SpeedTestMini(params[0], Integer.parseInt(params[1]));
            mini.setProgressReportListener(new ProgressReportListener() {
                long ul_Percentage = 0;
                long ulbits_last = 0;

                @Override
                public void reportCurrentDownloadSpeed(long bits) {
                }

                @Override
                public void reportCurrentUploadSpeed(long bits) {
                    SpeedTestUploadTask.this.publishProgress(bits, ul_Percentage);
                    ulbits_last = bits;
                    ul_avg = ul_avg + bits;
                    cycle = cycle + 1;
                }

                @Override
                public void reportCurrentDownloadProgress(long l) {

                }

                @Override
                public void reportCurrentUploadPercentage(long percentage) {
                    SpeedTestUploadTask.this.publishProgress(ulbits_last, percentage);
                    ul_Percentage = percentage;
                }

            });

            try {

                //mini.doDownloadtest();
                mini.doUploadtest();

                return 0L;
            } catch (IOException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }
            return 0L;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            String suffix = AppController.getInstance().getAppContext().getResources().getString(R.string.bits_sec);
            //double sout = values[0];
            double sout2 = values[0];
            long percentage = values[1];
            final double gval2 = sout2 / 1000000.0;
            //final double gval = sout / 1000000.0;
            if (sout2 > 1000000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.mbps);
                //sout /= 1000000.0;
                sout2 /= 1000000.0;
            } else if (sout2 > 1000) {
                suffix = " "+AppController.getInstance().getAppContext().getResources().getString(R.string.kbps);
                //sout /= 1000.0;
                sout2 /= 1000.0;
            }
            //Log.d("Speed",+ sout + " " + sout2);
            //download.setText("Download\n" + String.format("%.2f", sout) + suffix + "\n");
            upload.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.upload)+"\n" + String.format("%.2f", sout2) + suffix + "\n");
            circular.setProgress(100 - (int) percentage);

//            Log.d("uploadDb",circular.getProgress() + "");

//            circular.setTitle(String.format("%.2f", sout2) + suffix);
//            circular.setSubTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.upload));
            uploadDb=(String.format("%.2f", sout2)+suffix);
            //gauge.setValue(gval2);

            textviewStatus.setText(String.format("%.2f", sout2));
            textviewUnit.setText(suffix);
        }

        @Override
        protected void onPostExecute(Long result) {

            upload.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.NativeGreen));

            dateDb = new SimpleDateFormat("dd-MMM").format(new Date());
            timeDb = new SimpleDateFormat("HH:mm:ss").format(new Date());
            db.addGroup(new SpeedTestSQLiteData(dateDb, timeDb, downloadDb, uploadDb, pingDb));

            buttonTest.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.start_test));
            buttonTest.setVisibility(View.GONE);
            buttonFinished.setVisibility(View.VISIBLE);
            buttonShare.setVisibility(View.GONE);
            mLogoapp.setVisibility(View.GONE);

            showIntroSpeedtestHistory(buttonShare);

            TextView textview_result = (TextView) mRootView.findViewById(R.id.textview_result);
            textview_result.setVisibility(View.VISIBLE);

            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                //Show ads native
                RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
                adsNativeRootview.setVisibility(View.VISIBLE);

                //Gone Speed View
                RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
                speedTestFragment.setVisibility(View.GONE);

//                AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//                mAdView.setVisibility(View.GONE);
                adContainerView = mRootView.findViewById(R.id.ad_view_container);
                adContainerView.setVisibility(View.GONE);
                // Since we're loading the banner based on the adContainerView size, we need to wait until this
                // view is laid out before we can get the width.
                adContainerView.post(new Runnable() {
                    @Override
                    public void run() {
                        loadBanner();
                    }
                });
            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                //Show ads native
                RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
                adsNativeRootview.setVisibility(View.VISIBLE);

                //Gone Speed View
                RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
                speedTestFragment.setVisibility(View.GONE);


                adViewfacebook.setVisibility(View.GONE);
                adViewfacebook.loadAd();
            }


//            //Add animation for circular
//            circular.startAnimation(anim);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    circular.clearAnimation();
//                }
//            }, 3000);
//            circular.setEnabled(true);
        }
    }

    private class SpeedTestLatency extends AsyncTask<String, Void, Long> {

        @Override
        protected void onPreExecute() {
            display.setVisibility(View.VISIBLE);
            ping.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.ping)+"\n"+AppController.getInstance().getAppContext().getResources().getString(R.string.waiting));
            ping.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.orange));
            download.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.download)+"\n"+AppController.getInstance().getAppContext().getResources().getString(R.string.waiting));
            upload.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.upload)+"\n"+AppController.getInstance().getAppContext().getResources().getString(R.string.waiting));
//            buttonTest.startAnimation(anim);
            circular.setProgress(100);
            circular.setTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.latency_test));
            circular.startAnimation(anim);
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(String... params) {
            long dt = 3000;
            long avg = 0;
            for (int i = 0; i < 10; i++) {
                long t1 = System.nanoTime();
                try {
                    Socket socket = new Socket(InetAddress.getByName(host), 80);
                    long t2 = System.nanoTime();
                    dt = (t2 - t1) / 1000000;
                    avg = avg + dt;
                    socket.close();
                } catch (IOException e) {
                    Crashlytics.logException(e);
                }
            }
            dt = avg / 10;
            return dt;
        }

        @Override
        protected void onPostExecute(Long time) {
            pingDb = (Long.toString(time)+" "+AppController.getInstance().getAppContext().getResources().getString(R.string.ms));
            ping.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.ping)+"\n" + time + " " + AppController.getInstance().getAppContext().getResources().getString(R.string.ms));
            ping.setTextColor(AppController.getInstance().getAppContext().getResources().getColor(R.color.NativeGreen));
            circular.clearAnimation();
            super.onPostExecute(time);
            new SpeedTestDownloadTask().execute(host, "80");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.speed_test_mini, container,
                    false);

            serverXML = FeedXmlUtil.feed(getActivity());
            download = (TextView) mRootView.findViewById(R.id.textViewDownload);
            upload = (TextView) mRootView.findViewById(R.id.textViewUpload);
            ping = (TextView) mRootView.findViewById(R.id.textViewLatency);
            buttonTest = (Button) mRootView.findViewById(R.id.buttonTest);
            buttonFinished = (Button) mRootView.findViewById(R.id.buttonFinished);
            buttonShare = (Button) mRootView.findViewById(R.id.buttonShare);
            mLogoapp = (LinearLayout) mRootView.findViewById(R.id.logoapp);

            buttonFinished.setVisibility(View.GONE);
            buttonShare.setVisibility(View.GONE);
            mLogoapp.setVisibility(View.VISIBLE);

            display = (LinearLayout) mRootView.findViewById(R.id.measumentDisplay);
            speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
            circular = (CircularProgressBar) mRootView.findViewById(R.id.circularprogressbar);
            circular.setProgress(100);

            textviewStatus = (TextView) mRootView.findViewById(R.id.textview_status);
            textviewUnit = (TextView) mRootView.findViewById(R.id.textview_unit);


            connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(1000); //You can manage the blinking time with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);

            db = new SpeedTestSQLite(getActivity());

            buttonTest.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.start_test));
            buttonTest.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("Click Test Speed"));

//                    countSpeedTesting += 1; //count speed testing

                    circular.setVisibility(View.VISIBLE);
                    textviewStatus.setVisibility(View.VISIBLE);
                    textviewUnit.setVisibility(View.VISIBLE);
                    buttonTest.setVisibility(View.GONE);

                    host = AppUtils.speedtest_server;
                    new SpeedTestLatency().execute();
                }
            });
            buttonFinished.setText(AppController.getInstance().getAppContext().getResources().getString(R.string.test_again));
            buttonFinished.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    try {

                        buttonFinishedSelected();

//                    if(countSpeedTesting >= 3) {
//                        countSpeedTesting = 0;
                        //Show Full Ads
                        if(AppUtils.ads_interstitial_show_all) {

                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                                AppUtils.getInstance().showAdsFullBanner(null);
                            }

                        }else {

                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
                                AppUtils.getInstance().showAdmobAdsFullBanner(null);
                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
                                AppUtils.getInstance().showFBAdsFullBanner(null);
                            }
                        }
//                    }
                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                }
            });

            buttonShare.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    try {
                        if (ShareDialog.canShow(ShareLinkContent.class)) {

                            Context context = AppController.getInstance().getAppContext();
                            String setContentUrl = "";
                            String setImageUrl = "";

                            try{
                                if(AppUtils.appConfig != null){

                                    setContentUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                            .get_share_contentUrl();
                                    setImageUrl = AppUtils.appConfig.getData().getSpeedTestSharing()
                                            .get_share_setImageUrl();

                                } else{
                                    setContentUrl = context.getString(R.string.share_contentUrl);
                                    setImageUrl = context.getString(R.string.share_setImageUrl);
                                }
                            }catch (Exception e){
                                Crashlytics.logException(e);
                                setContentUrl = context.getString(R.string.share_contentUrl);
                                setImageUrl = context.getString(R.string.share_setImageUrl);
                            }


                            ShareLinkContent content = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse(setContentUrl))
                                    .setImageUrl(Uri.parse(setImageUrl))
                                    .setContentTitle("Best Free Speed Test Try it!\nDL: " + downloadDb
                                            + " | UL: " + uploadDb)
                                    .setContentDescription(context.getString(R.string.share_setContentDescription))
                                    .build();
                            mShareDialog.show(content);


                            Answers.getInstance().logShare(new ShareEvent()
                                    .putMethod("Facebook"));
                        }
                    } catch (Exception e) {
                        Crashlytics.logException(e);

                        Toast.makeText(getActivity(),"Share Error : Not Found Facebook Installed On Your Device.",Toast.LENGTH_SHORT).show();

                        Answers.getInstance().logShare(new ShareEvent()
                                .putMethod("Error"));
                    }

                }
            });

            buttonShowWifiSelected();

            mShareDialog = new ShareDialog(this);

//            //Click to speed booster
//            fab_speed_booster.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Speed Booster"));
//
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SPEEDBOOSTER_TYPE_KEY, true);
//                    editor.apply();
//
//                    Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                    intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                    startActivity(intent);
//                }
//            });


            //Init admob or facebook
            initAds();
        }


        return  mRootView;
    }


    private void buttonShowWifiSelected(){
        display.setVisibility(View.VISIBLE);
        speedTestFragment.setVisibility(View.VISIBLE);
        buttonTest.setVisibility(View.VISIBLE);
    }

    private void buttonFinishedSelected(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

            //Gone ads native
            RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
            adsNativeRootview.setVisibility(View.GONE);


            //Show Speed View
            RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
            speedTestFragment.setVisibility(View.VISIBLE);

            //Gone text result
            TextView textview_result = (TextView) mRootView.findViewById(R.id.textview_result);
            textview_result.setVisibility(View.GONE);

//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);

            adContainerView = mRootView.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
//            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

            //Gone ads native
            RelativeLayout adsNativeRootview = (RelativeLayout) mRootView.findViewById(R.id.adsNativeRootview);
            adsNativeRootview.setVisibility(View.GONE);

            //load new ads native
            LinearLayout adNativeViewContainer = (LinearLayout) mRootView.findViewById(R.id.facebook_native_ad_container);
            adNativeViewContainer.removeAllViews();
            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer);

            //Show Speed View
            RelativeLayout speedTestFragment = (RelativeLayout) mRootView.findViewById(R.id.speedTestFragment);
            speedTestFragment.setVisibility(View.VISIBLE);

            //Gone text result
            TextView textview_result = (TextView) mRootView.findViewById(R.id.textview_result);
            textview_result.setVisibility(View.GONE);

            adViewfacebook.setVisibility(View.VISIBLE);
        }


        circular.setVisibility(View.GONE);
        textviewStatus.setVisibility(View.GONE);
        textviewUnit.setVisibility(View.GONE);
        buttonTest.setVisibility(View.VISIBLE);

        buttonTest.setVisibility(View.VISIBLE);
        buttonFinished.setVisibility(View.GONE);
        buttonShare.setVisibility(View.GONE);
        mLogoapp.setVisibility(View.VISIBLE);

//        showIntroSpeedBooster(fab_speed_booster);
    }
    private com.facebook.ads.AdView adViewfacebook;
    private int countSpeedTesting;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
//            AdView mAdView = (AdView) mRootView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);


            adContainerView = mRootView.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });

            //Native
            refreshAd();


//            mPublisherAdView = mRootView.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mRootView.findViewById(R.id.admob_native_view);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);


        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads
//            AdSettings.addTestDevice("20493d3d20e3114b4db3dbefdde28996");

            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mRootView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mRootView.findViewById(R.id.facebook_native_ad_container);
            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer);

        }
    }


    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private void showDialog() {
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle(AppController.getInstance().getAppContext().getResources().getString(R.string.loading));
        dialog.setMessage(AppController.getInstance().getAppContext().getResources().getString(R.string.searching_location));
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
    }

    private void wifi_change(Boolean mWifi) {
        WifiManager mWifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mWifiManager.setWifiEnabled(mWifi);
    }

    private String host;
    private final LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(final Location location) {
            dialog.dismiss();
            CreateDatabaseAsynctask dbCreate = new CreateDatabaseAsynctask(getActivity());
            dbCreate.execute(location);
            try {
                nearestlist = dbCreate.get();
                //getServerData();
            } catch (InterruptedException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            } catch (ExecutionException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            } catch (Exception e){
                Crashlytics.logException(e);
            }
            mLocationManager.removeUpdates(mLocationListener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d("Speed test", "Status Changed");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d("Speed test", "Provider Enabled");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d("Speed test", "Provider Disabled");
        }
    };

    private void showIntroSpeedtestHistory(View view){

        try{
            if(view == null) return;

            if(sharedPref == null) return;

            if(getActivity() == null) return;

            if(sharedPref.getBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SHARE_TYPE_KEY, false)) {
                return;
            }

            new MaterialIntroView.Builder(getActivity())
//                    .enableDotAnimation(true)
                    .setTargetPadding(40)
                    .enableIcon(false)
                    .setFocusGravity(FocusGravity.CENTER)
                    .setFocusType(Focus.MINIMUM)
                    .setDelayMillis(500)
                    .enableFadeAnimation(true)
                    .performClick(true)
                    .setInfoText("Hi There! You can now share speed internet to " +
                            "your friend, what are you waiting for Share it now !")
                    .setShape(ShapeType.CIRCLE)
                    .setTarget(view)
                    .setUsageId("intro_speed_share") //THIS SHOULD BE UNIQUE ID
                    .dismissOnTouch(true);
//                    .show();


            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SHARE_TYPE_KEY, true);
            editor.apply();
        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }


    private void showIntroSpeedBooster(View view){

        try{
            if(view == null) return;

            if(sharedPref == null) return;

            if(getActivity() == null) return;

            if(sharedPref.getBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SPEEDBOOSTER_TYPE_KEY, false)) {
                return;
            }

            new MaterialIntroView.Builder(getActivity())
//                    .enableDotAnimation(true)
                    .setTargetPadding(40)
                    .enableIcon(false)
                    .setFocusGravity(FocusGravity.CENTER)
                    .setFocusType(Focus.MINIMUM)
                    .setDelayMillis(500)
                    .enableFadeAnimation(true)
                    .performClick(true)
                    .setInfoText("Hi There! You can now boost your internet speed, " +
                            "what are you waiting for boost it up now!")
                    .setShape(ShapeType.CIRCLE)
                    .setTarget(view)
                    .setUsageId("intro_speed_booster") //THIS SHOULD BE UNIQUE ID
                    .dismissOnTouch(true)
                    .show();


            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(AppUtils.SHOW_INTRO_SPEEDTEST_SPEEDBOOSTER_TYPE_KEY, true);
            editor.apply();

        }catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    /**
     * Populates a {@link UnifiedNativeAdView} object with data from a given
     * {@link UnifiedNativeAd}.
     *
     * @param nativeAd the object containing the ad's assets
     * @param adView          the view to be populated
     */
    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        try {
            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
            // have a video asset.
            VideoController vc = nativeAd.getVideoController();

            // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
            // VideoController will call methods on this object when events occur in the video
            // lifecycle.
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                public void onVideoEnd() {
                    // Publishers should allow native ads to complete video playback before refreshing
                    // or replacing them with another ad in the same UI location.
//                refresh.setEnabled(true);
//                videoStatus.setText("Video status: Video playback has ended.");
                    super.onVideoEnd();
                }
            });

            MediaView mediaView = adView.findViewById(R.id.ad_media);
            ImageView mainImageView = adView.findViewById(R.id.ad_image);

            // Apps can check the VideoController's hasVideoContent property to determine if the
            // NativeAppInstallAd has a video asset.
            if (vc.hasVideoContent()) {
                adView.setMediaView(mediaView);
                mainImageView.setVisibility(View.GONE);
//            videoStatus.setText(String.format(Locale.getDefault(),
//                    "Video status: Ad contains a %.2f:1 video asset.",
//                    vc.getAspectRatio()));
            } else {
                adView.setImageView(mainImageView);
                mediaView.setVisibility(View.GONE);

                // At least one image is guaranteed.
                List<NativeAd.Image> images = nativeAd.getImages();
                mainImageView.setImageDrawable(images.get(0).getDrawable());

//            refresh.setEnabled(true);
//            videoStatus.setText("Video status: Ad does not contain a video asset.");
            }

            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
            adView.setBodyView(adView.findViewById(R.id.ad_body));
            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(adView.findViewById(R.id.ad_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.ad_price));
            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
//        adView.setStoreView(adView.findViewById(R.id.ad_store));
            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

            // Some assets are guaranteed to be in every UnifiedNativeAd.
            ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
            // check before trying to display them.
            if (nativeAd.getIcon() == null) {
                adView.getIconView().setVisibility(View.GONE);
            } else {
                ((ImageView) adView.getIconView()).setImageDrawable(
                        nativeAd.getIcon().getDrawable());
                adView.getIconView().setVisibility(View.VISIBLE);
            }

//        if (nativeAd.getPrice() == null) {
//            adView.getPriceView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getPriceView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
//        }

//        if (nativeAd.getStore() == null) {
//            adView.getStoreView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getStoreView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
//        }

            if (nativeAd.getStarRating() == null) {
                adView.getStarRatingView().setVisibility(View.INVISIBLE);
            } else {
                ((RatingBar) adView.getStarRatingView())
                        .setRating(nativeAd.getStarRating().floatValue());
                adView.getStarRatingView().setVisibility(View.VISIBLE);
            }

            if (nativeAd.getAdvertiser() == null) {
                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
            } else {
                ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
                adView.getAdvertiserView().setVisibility(View.VISIBLE);
            }

            adView.setNativeAd(nativeAd);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    /**
     * Creates a request for a new native ad based on the boolean parameters and calls the
     * corresponding "populate" method when one is successfully returned.
     */
    private void refreshAd() {
        try {
//        refresh.setEnabled(false);



            AdLoader.Builder builder2 = new AdLoader.Builder(getActivity(), getResources().getString(R.string.admob_large_native_id));

            builder2.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                // OnUnifiedNativeAdLoadedListener implementation.
//                @Override
//                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd2) {
//                    try {
//                        FrameLayout frameLayout2 =
//                                mView.findViewById(R.id.fl_adplaceholder2);
//                        UnifiedNativeAdView adView2 = (UnifiedNativeAdView) getLayoutInflater()
//                                .inflate(R.layout.ad_unified, null);
//                        populateUnifiedNativeAdView(unifiedNativeAd2, adView2);
//                        frameLayout2.removeAllViews();
//                        frameLayout2.addView(adView2);
//                    } catch (Exception ignored) {
//                        Crashlytics.logException(ignored);
//                    }
//                }
                @Override
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    NativeTemplateStyle styles = new
                            NativeTemplateStyle.Builder().build();

                    TemplateView template = mRootView.findViewById(R.id.my_template);
                    template.setStyles(styles);
                    template.setNativeAd(unifiedNativeAd);
                    template.setVisibility(View.VISIBLE);
                }

            });


            VideoOptions videoOptions = new VideoOptions.Builder()
                    .build();

            NativeAdOptions adOptions = new NativeAdOptions.Builder()
                    .setVideoOptions(videoOptions)
                    .build();

            builder2.withNativeAdOptions(adOptions);

            AdLoader adLoader2 = builder2.withAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
//                refresh.setEnabled(true);
//                Toast.makeText(MainActivity.this, "Failed to load native ad: "
//                        + errorCode, Toast.LENGTH_SHORT).show();
                }
            }).build();

            adLoader2.loadAd(new AdRequest.Builder().build());

//        videoStatus.setText("");

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (adtView != null) {
            adtView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (adtView != null) {
            adtView.resume();
        }
    }
    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(Objects.requireNonNull(getActivity()));
        adtView.setAdUnitId(getResources().getString(R.string.admob_adaptive_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);

        return AdSize.getCurrentOrientationBannerAdSizeWithWidth(getActivity(), adWidth);
    }
}
