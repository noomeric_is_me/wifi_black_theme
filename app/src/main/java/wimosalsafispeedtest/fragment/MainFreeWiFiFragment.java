package wimosalsafispeedtest.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import java.util.List;
import java.util.Objects;

import wimosalsafifreewifi.activity.ChartActivity;
import wimosalsafifreewifi.activity.SpeedTestMiniActivity;
import wimosalsafifreewifi.activity.WifiConnectActivity;
import wimosalsafifreewifi.application.AppController;
import wimosalsafifreewifi.main.MainAppActivity;
import wimosalsafifreewifi.main.MapsMarkerActivity;
import wimosalsafimainapp.scrollable.fragment.FragmentPagerFragment;
import wimosalsafispeedtest.activity.OptimizeActivity;
import utils.AppUtils;
import wimosalsafiwifimap.activity.MapActivity;

import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;

/**
 * Created by NTL on 8/12/2017 AD.
 */

public class MainFreeWiFiFragment extends FragmentPagerFragment {

    public static final String TAG = MainFreeWiFiFragment.class.getSimpleName();
    private FrameLayout adContainerView;
    private AdView adtView;
    private View mView;
    private TextView text_number_wifi_in_range;
    private NestedScrollView mScrollView;
    private MainListener mMainListener;
    private PublisherAdView mPublisherAdView;
    public interface MainListener{
        void clickHotspot();
    }

    public void setMainListener(MainListener mainListener){
        mMainListener = mainListener;
    }

    public static MainFreeWiFiFragment newInstance(){
        return new MainFreeWiFiFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mView == null){
            mView = inflater.inflate(R.layout.main_vmkoomfreewifi_ffragment2, container, false);

//            mScrollView = mView.findViewById(R.id.scrollView);

            mScrollView = (NestedScrollView) mView.findViewById(R.id.scrollView);
//            MaterialViewPagerHelper.registerScrollView(getActivity(), mScrollView);

            text_number_wifi_in_range = (TextView) mView.findViewById(R.id.text_number_wifi_in_range);

            TextView txt_wifi_connect = (TextView) mView.findViewById(R.id.txt_wifi_connect);
            txt_wifi_connect.setOnClickListener(mClick);

            TextView txt_wifi_speed_boost = (TextView) mView.findViewById(R.id.txt_wifi_speed_boost);
            txt_wifi_speed_boost.setOnClickListener(mClick);

            TextView txt_wifi_chart = (TextView) mView.findViewById(R.id.txt_wifi_analyzer);
            txt_wifi_chart.setOnClickListener(mClick);

            TextView txt_wifi_map = (TextView) mView.findViewById(R.id.txt_wifi_map);
            txt_wifi_map.setOnClickListener(mClick);

            TextView txt_wifi_speed_test = (TextView) mView.findViewById(R.id.txt_wifi_speed_test);
            txt_wifi_speed_test.setOnClickListener(mClick);

            TextView txt_wifi_hotspot = (TextView) mView.findViewById(R.id.txt_wifi_hotspot);
            txt_wifi_hotspot.setOnClickListener(mClick);

            TextView txt_wifi_map_explorer = (TextView) mView.findViewById(R.id.txt_wifi_map_explorer);
            txt_wifi_map_explorer.setOnClickListener(mClick);

            initAds();
            MobileAds.initialize(getActivity(), getResources().getString(R.string.admob_app_id));
            refreshAd();
        }

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onDestroy() {
        if(adViewfacebook != null){
            adViewfacebook.destroy();
        }
        super.onDestroy();
    }

    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) mView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            adContainerView = mView.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
//            mPublisherAdView = mView.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

//            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view);
//            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest);
//
//            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view_1);
//            PublisherAdRequest publisherAdRequest_1 = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest_1);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mView.findViewById(R.id.native_ad_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeSmallInit(getActivity(),adNativeViewContainer);

            LinearLayout adNativeViewContainer_1 = (LinearLayout) mView.findViewById(R.id.native_ad_container_1);
            adNativeViewContainer_1.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer_1);
        }
    }


    public void setWiFiNumberInRange(String numbers){
        if(text_number_wifi_in_range != null){
            text_number_wifi_in_range.setText(numbers);
        }
    }

    public void setWiFiHotspotMenuText(String menuText){
        if(mView != null){
            TextView txt_wifi_hotspot = (TextView) mView.findViewById(R.id.txt_wifi_hotspot);
            if(txt_wifi_hotspot != null){
                txt_wifi_hotspot.setText(menuText);
            }
        }
    }

    private View.OnClickListener mClick = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            final Intent intent;
            int id = view.getId();
            switch (id) {
                case R.id.txt_wifi_connect:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Fine More Networks"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), WifiConnectActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
                case R.id.txt_wifi_speed_boost:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wifi Speed Booster"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), OptimizeActivity.class);
//                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
//                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), OptimizeActivity.class);
//                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), OptimizeActivity.class);
                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
                case R.id.txt_wifi_analyzer:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wifi Analyzer"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), ChartActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), ChartActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), ChartActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                    break;
                case R.id.txt_wifi_map:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Network Map Location"));

                        final double lat;
                        final double lng;

                        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
                            lat = 0;
                            lng = 0;
                        }else {
                            lat = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
                            lng = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
                        }

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
                case R.id.txt_wifi_map_explorer:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wi-Fi Map Explorer"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                    break;
                case R.id.txt_wifi_speed_test:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wifi Speed Test"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;

                case R.id.txt_wifi_hotspot:

                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Free WiFi Hotspot"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else {
//                                if(mMainListener != null){
//                                    mMainListener.clickHotspot();
//                                }
//
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else {
//                                if(mMainListener != null){
//                                    mMainListener.clickHotspot();
//                                }
//
//                            }
//                        }

                        if(mMainListener != null){
                            mMainListener.clickHotspot();
                        }

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;

            }
        }
    };

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }
    /**
     * Populates a {@link UnifiedNativeAdView} object with data from a given
     * {@link UnifiedNativeAd}.
     *
     * @param nativeAd the object containing the ad's assets
     * @param adView   the view to be populated
     */
    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        try {
            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
            // have a video asset.
            VideoController vc = nativeAd.getVideoController();

            // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
            // VideoController will call methods on this object when events occur in the video
            // lifecycle.
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                public void onVideoEnd() {
                    // Publishers should allow native ads to complete video playback before refreshing
                    // or replacing them with another ad in the same UI location.
//                refresh.setEnabled(true);
//                videoStatus.setText("Video status: Video playback has ended.");
                    super.onVideoEnd();
                }
            });

            MediaView mediaView = adView.findViewById(R.id.ad_media);
            ImageView mainImageView = adView.findViewById(R.id.ad_image);

            // Apps can check the VideoController's hasVideoContent property to determine if the
            // NativeAppInstallAd has a video asset.
            if (vc.hasVideoContent()) {
                adView.setMediaView(mediaView);
                mainImageView.setVisibility(View.GONE);
//            videoStatus.setText(String.format(Locale.getDefault(),
//                    "Video status: Ad contains a %.2f:1 video asset.",
//                    vc.getAspectRatio()));
            } else {
                adView.setImageView(mainImageView);
                mediaView.setVisibility(View.GONE);

                // At least one image is guaranteed.
                List<NativeAd.Image> images = nativeAd.getImages();
                mainImageView.setImageDrawable(images.get(0).getDrawable());

//            refresh.setEnabled(true);
//            videoStatus.setText("Video status: Ad does not contain a video asset.");
            }

            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
            adView.setBodyView(adView.findViewById(R.id.ad_body));
            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(adView.findViewById(R.id.ad_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.ad_price));
            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
//        adView.setStoreView(adView.findViewById(R.id.ad_store));
            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

            // Some assets are guaranteed to be in every UnifiedNativeAd.
            ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
            // check before trying to display them.
            if (nativeAd.getIcon() == null) {
                adView.getIconView().setVisibility(View.GONE);
            } else {
                ((ImageView) adView.getIconView()).setImageDrawable(
                        nativeAd.getIcon().getDrawable());
                adView.getIconView().setVisibility(View.VISIBLE);
            }

//        if (nativeAd.getPrice() == null) {
//            adView.getPriceView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getPriceView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
//        }

//        if (nativeAd.getStore() == null) {
//            adView.getStoreView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getStoreView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
//        }

            if (nativeAd.getStarRating() == null) {
                adView.getStarRatingView().setVisibility(View.INVISIBLE);
            } else {
                ((RatingBar) adView.getStarRatingView())
                        .setRating(nativeAd.getStarRating().floatValue());
                adView.getStarRatingView().setVisibility(View.VISIBLE);
            }

            if (nativeAd.getAdvertiser() == null) {
                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
            } else {
                ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
                adView.getAdvertiserView().setVisibility(View.VISIBLE);
            }

            adView.setNativeAd(nativeAd);

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    /**
     * Creates a request for a new native ad based on the boolean parameters and calls the
     * corresponding "populate" method when one is successfully returned.
     */
    private void refreshAd() {
        try {
//        refresh.setEnabled(false);

            AdLoader.Builder builder = new AdLoader.Builder(getActivity(), getResources().getString(R.string.admob_small_native_id));

            builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                // OnUnifiedNativeAdLoadedListener implementation.
                @Override
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    try {
                        FrameLayout frameLayout =
                                mView.findViewById(R.id.fl_adplaceholder);
                        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                                .inflate(R.layout.ad_unified2, null);
                        populateUnifiedNativeAdView(unifiedNativeAd, adView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(adView);
                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                }

            });

            AdLoader.Builder builder2 = new AdLoader.Builder(getActivity(), getResources().getString(R.string.admob_large_native_id));

            builder2.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                // OnUnifiedNativeAdLoadedListener implementation.
//                @Override
//                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd2) {
//                    try {
//                        FrameLayout frameLayout2 =
//                                mView.findViewById(R.id.fl_adplaceholder2);
//                        UnifiedNativeAdView adView2 = (UnifiedNativeAdView) getLayoutInflater()
//                                .inflate(R.layout.ad_unified, null);
//                        populateUnifiedNativeAdView(unifiedNativeAd2, adView2);
//                        frameLayout2.removeAllViews();
//                        frameLayout2.addView(adView2);
//                    } catch (Exception ignored) {
//                        Crashlytics.logException(ignored);
//                    }
//                }
                @Override
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    NativeTemplateStyle styles = new
                            NativeTemplateStyle.Builder().build();

                    TemplateView template = mView.findViewById(R.id.my_template);
                    template.setStyles(styles);
                    template.setNativeAd(unifiedNativeAd);
                    template.setVisibility(View.VISIBLE);
                }

            });


            VideoOptions videoOptions = new VideoOptions.Builder()
                    .build();

            NativeAdOptions adOptions = new NativeAdOptions.Builder()
                    .setVideoOptions(videoOptions)
                    .build();
            builder.withNativeAdOptions(adOptions);
            AdLoader adLoader = builder.withAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
//                refresh.setEnabled(true);
//                Toast.makeText(MainActivity.this, "Failed to load native ad: "
//                        + errorCode, Toast.LENGTH_SHORT).show();
                }
            }).build();
            builder2.withNativeAdOptions(adOptions);

            AdLoader adLoader2 = builder2.withAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
//                refresh.setEnabled(true);
//                Toast.makeText(getActivity(), "Failed to load native ad: "
//                        + errorCode, Toast.LENGTH_SHORT).show();
                }
            }).build();

            adLoader.loadAd(new AdRequest.Builder().build());
            adLoader2.loadAd(new AdRequest.Builder().build());

//        videoStatus.setText("");

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }
    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(Objects.requireNonNull(getActivity()));
        adtView.setAdUnitId(getResources().getString(R.string.admob_adaptive_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);

        return AdSize.getCurrentOrientationBannerAdSizeWithWidth(getActivity(), adWidth);
    }
}
