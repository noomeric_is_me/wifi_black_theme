package wimosalsafispeedtest.speedtestminilib;

public interface ProgressReportListener {
	public void reportCurrentDownloadSpeed(long bits);
	public void reportCurrentUploadSpeed(long bits);
	public void reportCurrentDownloadProgress(long l);
	public void reportCurrentUploadPercentage(long l);
}
