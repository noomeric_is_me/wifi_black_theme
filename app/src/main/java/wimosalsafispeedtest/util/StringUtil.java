package wimosalsafispeedtest.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Patterns;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

  public static boolean isURL(final String _string) {
    final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

    Pattern p = Pattern.compile(URL_REGEX);
    Matcher m = p.matcher(_string);// replace with string to compare
    if (m.find()) {
      return true;
    }
    return false;
  }

  public static CharSequence trimTrailingWhitespace(CharSequence source) {

    if (source == null)
      return "";

    int i = source.length();

    // loop back to the first non-whitespace character
    while (--i >= 0 && Character.isWhitespace(source.charAt(i))) {
    }

    return source.subSequence(0, i + 1);
  }

  public static String md5(String s) {
    try {
      // Create MD5 Hash
      MessageDigest digest = MessageDigest.getInstance("MD5");
      digest.update(s.getBytes());
      byte messageDigest[] = digest.digest();

      // Create Hex String
      StringBuffer hexString = new StringBuffer();
      for (int i = 0; i < messageDigest.length; i++)
        hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
      return hexString.toString();

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return "";
  }

  public static String getDeviceID(Context mContext) {
    // Get ID as admob
    String aid = Settings.Secure.getString(mContext.getContentResolver(), "android_id");
    Object obj = null;
    try {
      ((MessageDigest) (obj = MessageDigest.getInstance("MD5"))).update(aid.getBytes(), 0, aid.length());

      obj = String.format("%032X", new Object[] { new BigInteger(1, ((MessageDigest) obj).digest()) });
    } catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {
      obj = aid.substring(0, 32);
    }
    // End

    TelephonyManager phonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);

    String id = phonyManager.getDeviceId();
    if (id == null) {
      id = obj.toString();
    }

    int phoneType = phonyManager.getPhoneType();
    switch (phoneType) {
    case TelephonyManager.PHONE_TYPE_NONE:
      return "NONIMEI" + id;

    case TelephonyManager.PHONE_TYPE_GSM:
      return "IMEI" + id;

    case TelephonyManager.PHONE_TYPE_CDMA:
      return "MEIDESN" + id;

      /*
       * for API Level 11 or above case TelephonyManager.PHONE_TYPE_SIP: return
       * "SIP";
       */
    default:
      return "ANDID=" + id;
    }

  }

  public static String getGmail(Context mContext) {
    Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
    Account[] accounts = AccountManager.get(mContext).getAccounts();
    String possibleEmail = null;
    for (Account account : accounts) {
      if (emailPattern.matcher(account.name).matches()) {
        possibleEmail = account.name;
        if (possibleEmail.contains("@gmail.com"))
          break;
      }
    }
    return possibleEmail;
  }

  public static String convertStreamToStringV2(InputStream is, String backupDirPath, String backupFileName) {

    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();
    String line = null;
    BufferedWriter bufferWritter = null;
    try {
      // create backup file
      if (backupDirPath != null && backupFileName != null) {
        File directory = new File(backupDirPath);
        if (!directory.exists()) {
          directory.mkdirs();
        }
        File backupFile = new File(backupDirPath + "/" + backupFileName);
        bufferWritter = new BufferedWriter(new FileWriter(backupFile, false));

      }

      while ((line = reader.readLine()) != null) {
        sb.append(line).append("\n");

        // write backup file
        if (bufferWritter != null) {
          bufferWritter.append(line);
          bufferWritter.newLine();
        }
      }

      // close backup file
      if (bufferWritter != null) {
        bufferWritter.close();
      }

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        is.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return sb.toString();
  }

  public static ArrayList<String> getArrayStringFromURL(String url) {
    InputStream is = null;
    HttpClient httpclient = new DefaultHttpClient();
    HttpGet httpget = new HttpGet(url);
    HttpResponse response;
    ArrayList<String> result = new ArrayList<String>();
    try {
      response = httpclient.execute(httpget);
      HttpEntity entity = response.getEntity();
      if (entity != null) {
        is = entity.getContent();

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line = null;

        while ((line = reader.readLine()) != null) {
          result.add(line);
        }
      }
    } catch (IOException e) {
      return null;
    } finally {
      try {
        is.close();
      } catch (IOException e) {
        return null;
      }
    }

    return result;
  }

  public static String convertStreamToString(InputStream is) {
    /*
     * To convert the InputStream to String we use the BufferedReader.readLine()
     * method. We iterate until the BufferedReader return null which means
     * there's no more data to read. Each line will appended to a StringBuilder
     * and returned as String.
     */
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();
    String line = null;
    try {
      while ((line = reader.readLine()) != null) {
        sb.append(line + "\n");
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        is.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return sb.toString();
  }

  public static String getTimeStamp() {
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SS", Locale.getDefault());
    String dateString = sdf.format(new Date(System.currentTimeMillis()));
    return dateString;
  }

  public static String getCurrentDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String currentDate = sdf.format(new Date());
    return currentDate;
  }

  public static boolean checkEmailFormat(String _email) {

    Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
    Matcher m = p.matcher(_email);
    boolean matchFound = m.matches();

    if (matchFound) {
      return true;
    } else {
      return false;
    }
  }

  public static String getTimestampChat() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    return sdf.format(new Date(System.currentTimeMillis()));
  }

  public static String getTimeChat() {
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
    return sdf.format(new Date(System.currentTimeMillis()));
  }

  public static String getTimestamp2TimeChat(String timestamp) {
    String[] parts = timestamp.split(" ");
    String time = parts[1];
    String[] split = time.split(":");
    return (split[0] + ":" + split[1]);
  }

  public static String convert24to12(String time) {
    String convertedTime = "";
    try {
      SimpleDateFormat displayFormat = new SimpleDateFormat("h:mm a");
      SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
      Date date = parseFormat.parse(time);
      convertedTime = displayFormat.format(date);
    } catch (final ParseException e) {
      e.printStackTrace();
    }
    return convertedTime;
  }

  public static String convertTimestamp2ChatFormat(String timestamp) {
    String convertedTime = "";
    try {
      SimpleDateFormat displayFormat = new SimpleDateFormat("MMMM dd, yyyy, h:mm a");
      SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date date = parseFormat.parse(timestamp);
      convertedTime = displayFormat.format(date);
    } catch (final ParseException e) {
      e.printStackTrace();
    }
    return convertedTime;
  }

  public static String convertTimestamp2HHMM(String timestamp) {
    String convertedTime = "";
    try {
      SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
      SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date date = parseFormat.parse(timestamp);
      convertedTime = displayFormat.format(date);
    } catch (final ParseException e) {
      e.printStackTrace();
    }
    return convertedTime;
  }

  public static boolean isAddTime(String time1, String time2) {
    try {
      long TWO_MINUTES = 1000 * 60 * 2; // 2 minutes in milliseconds
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
      Date date1 = sdf.parse(time1);
      Date date2 = sdf.parse(time2);

      long mill1 = date1.getTime();
      long mill2 = date2.getTime();

      if (mill1 - mill2 < TWO_MINUTES && mill1 - mill2 > -TWO_MINUTES) {
        return false;
      } else {
        return true;
      }
    } catch (final ParseException e) {
      e.printStackTrace();
    }
    return false;
  }

  public static String getYoutubeVideoId(String youtubeUrl) {
    String video_id = "";
    if (youtubeUrl != null && youtubeUrl.trim().length() > 0 && youtubeUrl.startsWith("http")) {

      String expression = "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var
                                                                                                                            // regExp
                                                                                                                            // =
                                                                                                                            // /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
      CharSequence input = youtubeUrl;
      Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
      Matcher matcher = pattern.matcher(input);
      if (matcher.matches()) {
        String groupIndex1 = matcher.group(7);
        if (groupIndex1 != null && groupIndex1.length() == 11)
          video_id = groupIndex1;
      }
    }
    return video_id;
  }

  public static boolean isAudioURL(String audioUrl) {
    String ext = audioUrl.substring(audioUrl.lastIndexOf(".") + 1);
    if (ext.equals("3gp") || ext.equals("mp3")) {
      return true;
    }
    return false;

  }

  public static boolean isCallRequest(String _text) {

    if (_text.contains("CALL_REQ")) {
      return true;
    }
    return false;

  }

  public static boolean isCallAccept(String _text) {

    if (_text.contains("CALL_ACCEPT")) {
      return true;
    }
    return false;

  }

  public static boolean isCallDeny(String _text) {

    if (_text.contains("CALL_DENY")) {
      return true;
    }
    return false;

  }

  public static boolean isCallCharger(String _text) {
    if (_text.contains("CHARGER_CREDIT")) {
      return true;
    }
    return false;
  }

  public static boolean isURLofImage(String url) {
    String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(tif|tiff|jpg|jpeg|gif|png|bmp|bmpf|ico|cur|xbm))$)";
    Pattern pattern = Pattern.compile(IMAGE_PATTERN);
    Matcher matcher = pattern.matcher(url);
    return matcher.matches();
  }
}
