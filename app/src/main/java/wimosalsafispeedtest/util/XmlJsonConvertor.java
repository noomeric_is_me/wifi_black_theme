package wimosalsafispeedtest.util;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;
import org.w3c.dom.Element;

public class XmlJsonConvertor {

    public static String getValue(Object element, String key) {
        try {
            if (element instanceof JSONObject) {
                return ((JSONObject) element).getString(key);
            } else if (element instanceof Element) {
                return (String) FeedXmlUtil.getValue((Element) element, key);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return null;
    }
}
