package wimosalsafispeedtest.util;

import android.content.Context;


import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class FeedXmlUtil {

    public static XMLParser parser;

    public static ArrayList<Object> feed(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.speedtest_servers_static);
            ArrayList<Object> _feedList = new ArrayList<Object>();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = null;
            Document document = null;
            try {
                documentBuilder = documentBuilderFactory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            try {
                document = documentBuilder.parse(inputStream);
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            document.getDocumentElement().normalize();
            NodeList nl = document.getElementsByTagName("server");
            for (int j = 0; j < nl.getLength(); j++) {
                Element e = (Element) nl.item(j);
                _feedList.add(e);
            }
            return _feedList;

    }

    public static String getValue(Element e, String key){
        return parser.getValue(e, key);
    }
}
