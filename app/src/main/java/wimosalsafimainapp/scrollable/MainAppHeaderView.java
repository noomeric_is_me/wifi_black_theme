package wimosalsafimainapp.scrollable;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.wimosalsafi.wifi.password.anywhere.map.connection.hotspot.wifianalyzer.R;

public class MainAppHeaderView extends FrameLayout {

//    private TextView mTextView;

    private int mCollapsedColor;
    private int mExpandedColor;
    private View mView;

    public MainAppHeaderView(Context context) {
        this(context, null);
    }

    public MainAppHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainAppHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {

        mView = inflate(context, R.layout.view_header, this);

//        mTextView = ViewUtils.findView(this, R.id.text_view);

        if (attributeSet != null) {
            final TypedArray array = context.obtainStyledAttributes(attributeSet, R.styleable.MainAppHeaderView);
            try {
                mCollapsedColor = array.getColor(R.styleable.MainAppHeaderView_shv_collapsedColor, 0);
                mExpandedColor = array.getColor(R.styleable.MainAppHeaderView_shv_expandedColor, 0);

//                mTextView.setText(array.getString(R.styleable.SampleHeaderView_shv_title));
            } finally {
                array.recycle();
            }
        }
    }

//    public TextView getTextView() {
//        return mTextView;
//    }

    public View getRootView() {
        return mView;
    }

    public int getCollapsedColor() {
        return mCollapsedColor;
    }

    public int getExpandedColor() {
        return mExpandedColor;
    }
}
