# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-ignorewarnings
#-dontobfuscate

# Class of App
-keep class wimosalsafifreewifi.services.model.** {*;}

-keep class android.support.design.widget.** { *; }
-keep interface android.support.design.widget.** { *; }
-dontwarn android.support.design.**

# Jackson
-dontoptimize
-dontshrink
-keepattributes *Annotation*,EnclosingMethod,SourceFile,LineNumberTable


-keepclassmembers enum  * {
    *;
}

# fabric (Crashlytic)
-keep class com.crashlytics.** { *; }
-keep class com.crashlytics.android.**


# AppCompat
-keep class android.support.v13.app.** { *; }
-keep class android.support.v7.app.** { *; }
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }

#
# Google Analytics Start
#
-keep public class com.google.android.gms.analytics.** {
    public *;
}

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
#
# Google Analytics End
#


-dontwarn org.codehaus.jackson.map.ext.**
-dontwarn com.malinskiy.**
-dontwarn com.octo.android.robospice.**
-dontwarn org.springframework.http.**
-dontwarn com.flurry.sdk.**
-dontwarn org.objenesis.**
-keep class org.apache.** { *; }
-keep class org.codehaus.** { *; }
-keep class com.octo.** { *; }
-keep class org.springframework.** { *; }
-keep class com.jakewharton.** { *; }



-keep class com.github.bumptech.glide.** { *; }
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}

#modify for Facebook
-keepattributes Signature
-keep class com.facebook.** { *; }

-keepnames class * implements java.io.Serializable
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}


-keep class com.bumptech.glide.integration.volley.VolleyGlideModule

# see https://github.com/square/okio/issues/60
-keep public class org.codehaus.**
-keep public class java.nio.**
-dontwarn com.squareup.**
-dontwarn okio.**

#retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keep class com.romainpiel.** { *; }

-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**
-keep public class com.google.android.gms.* { public *; }

-keep class cn.pedant.SweetAlert.Rotate3dAnimation {
    public <init>(...);
}

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}